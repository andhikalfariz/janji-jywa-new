package model;
import java.time.LocalDateTime;

public class HeaderTransaction {


	private String id;
	private String userId;
	private String date;
	
	public HeaderTransaction() {
		
	}

	
	public HeaderTransaction(String id, String userId, String date) {
		this.id = id;
		this.userId = userId;
		this.date = date;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getDate() {
		return date;
	}


	public void setDate(String date) {
		this.date = date;
	}
	
	

}
