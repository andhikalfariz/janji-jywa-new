package model;

public class Cart {


	private String userId;
	private String beverageId;
	private String beverageName;
	private String beveragetype;
	private int beveragePrice;
	private int beverageStock;
	private int quantity;
	private int subtotal;
	
	
	public Cart() {}
	
	public Cart(String userId, String beverageId, String beverageName, String beveragetype, int beveragePrice,
			int beverageStock, int quantity, int subtotal) {
		super();
		this.userId = userId;
		this.beverageId = beverageId;
		this.beverageName = beverageName;
		this.beveragetype = beveragetype;
		this.beveragePrice = beveragePrice;
		this.beverageStock = beverageStock;
		this.quantity = quantity;
		this.subtotal = subtotal;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBeverageId() {
		return beverageId;
	}

	public void setBeverageId(String beverageId) {
		this.beverageId = beverageId;
	}

	public String getBeverageName() {
		return beverageName;
	}

	public void setBeverageName(String beverageName) {
		this.beverageName = beverageName;
	}

	public String getBeveragetype() {
		return beveragetype;
	}

	public void setBeveragetype(String beveragetype) {
		this.beveragetype = beveragetype;
	}

	public int getBeveragePrice() {
		return beveragePrice;
	}

	public void setBeveragePrice(int beveragePrice) {
		this.beveragePrice = beveragePrice;
	}

	public int getBeverageStock() {
		return beverageStock;
	}

	public void setBeverageStock(int beverageStock) {
		this.beverageStock = beverageStock;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(int subtotal) {
		this.subtotal = subtotal;
	}
	
	


	
	
	
	
	
}
