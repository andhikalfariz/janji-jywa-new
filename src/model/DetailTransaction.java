package model;

public class DetailTransaction {


	private int quantity;
	private String headerTransactionId;
	private String beverageId;
	private String beverageName;
	private String beveragetype;
	private int beveragePrice;
	private int subtotal;
	
	
	public DetailTransaction() {}


	public DetailTransaction(int quantity, String headerTransactionId, String beverageId, String beverageName,
			String beveragetype, int beveragePrice, int subtotal) {
		this.quantity = quantity;
		this.headerTransactionId = headerTransactionId;
		this.beverageId = beverageId;
		this.beverageName = beverageName;
		this.beveragetype = beveragetype;
		this.beveragePrice = beveragePrice;
		this.subtotal = subtotal;
	}


	public int getQuantity() {
		return quantity;
	}


	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}


	public String getHeaderTransactionId() {
		return headerTransactionId;
	}


	public void setHeaderTransactionId(String headerTransactionId) {
		this.headerTransactionId = headerTransactionId;
	}


	public String getBeverageId() {
		return beverageId;
	}


	public void setBeverageId(String beverageId) {
		this.beverageId = beverageId;
	}


	public String getBeverageName() {
		return beverageName;
	}


	public void setBeverageName(String beverageName) {
		this.beverageName = beverageName;
	}


	public String getBeveragetype() {
		return beveragetype;
	}


	public void setBeveragetype(String beveragetype) {
		this.beveragetype = beveragetype;
	}


	public int getBeveragePrice() {
		return beveragePrice;
	}


	public void setBeveragePrice(int beveragePrice) {
		this.beveragePrice = beveragePrice;
	}


	public int getSubtotal() {
		return subtotal;
	}


	public void setSubtotal(int subtotal) {
		this.subtotal = subtotal;
	}
	
	
	
	
	
	
}
