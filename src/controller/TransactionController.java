package controller;


import utill.MySQLConnection;

import java.util.List;

import model.DetailTransaction;
import model.HeaderTransaction;
import model.Users;

import java.util.ArrayList;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TransactionController {
	
	private static TransactionController instance = null;
	private List<HeaderTransaction> transactionList = null;
	private List<DetailTransaction> detailTransactionList = null;
	static Users userlog;
	static UsersController userController = UsersController.getInstance();
	
	private TransactionController(){
		transactionList = new ArrayList<>();
		detailTransactionList = new ArrayList<>();
		
		userlog = userController.getLoggedUser();
		
		
		try {
			transactionList = getTransactionByUserId(userlog.getId());
			detailTransactionList = getAllDetailTransaction();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static TransactionController getInstance()
    {
        if (instance == null)
            instance = new TransactionController();
 
        return instance;
    }
	
	public List<String> fetchAllId(){
		List<String> transactionId = new ArrayList<>();
		String query = "SELECT TransactionID FROM headertransactions";
		
		try(Connection connection = MySQLConnection.createConnection()){
			try(PreparedStatement ps = connection.prepareStatement(query)){
				try(ResultSet rs = ps.executeQuery()){
					while(rs.next()) {
						String id = rs.getString("TransactionID");
						transactionId.add(id);
					}
				}
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return transactionId;
	}
	
	public String generateTransactionId() {
		List<String> transactionId;
		String lastID = "TR001";
		
		transactionId = fetchAllId();
		
		if(transactionId.size() != 0 ) {
			lastID = transactionId.get(transactionId.size()-1);
		}
		
		int newIdNumber = Integer.parseInt(lastID.substring(2)) + 1;
		
		return String.format("TR%03d" , newIdNumber);
	}
	
	public void addDetailTransaction(String transactionID, List<String> listId, List<Integer> listQuantity) {
		try(Connection connection = MySQLConnection.createConnection()){
			Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			
			String query1 = "SET SQL_SAFE_UPDATES = 0";
			stmt.addBatch(query1);
			
			for(int i=0; i< listId.size(); i++) {
				String query = String.format("INSERT INTO detailtransactions (TransactionID,BeverageID,Quantity) VALUES ('%s', '%s', '%s');"
						, transactionID, listId.get(i), listQuantity.get(i));
				stmt.addBatch(query);
				
			}
		    
		    stmt.executeBatch();
		    connection.commit();
			
		    detailTransactionList = getAllDetailTransaction();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	public String addHeaderTransaction() {
		String userId = userlog.getId(); //diganti nanti
		String transactionID = generateTransactionId();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("uuuu-MM-dd");
		LocalDate localDate = LocalDate.now();
		String transactionDate = dtf.format(localDate);
		
		
		try(Connection connection = MySQLConnection.createConnection()){
			Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			
			String query1 = "SET SQL_SAFE_UPDATES = 0";
			String query2 = String.format("INSERT INTO headertransactions (TransactionID,UserID,TransactionDate) VALUES ('%s', '%s', '%s');"
					, transactionID, userId, transactionDate);
			
			stmt.addBatch(query1);
		    stmt.addBatch(query2);
		    
		    stmt.executeBatch();
		    connection.commit();
			
		    transactionList = getTransactionByUserId(userlog.getId());//nanti diganti
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return transactionID;
		
	}
	
	public List<HeaderTransaction> getTransactionList(){
		return transactionList;
	}
	
	public List<DetailTransaction> getDetailTransactionList(){
		return detailTransactionList;
	}
	
	public int getTotalDetailTransactionPrice(String trId) {
		int total = 0;
		
		for(DetailTransaction dt : detailTransactionList) {
			if(dt.getHeaderTransactionId().equals(trId) ) {
				total += dt.getSubtotal();
			}
		}
		
		return total;
	}
	
	public List<DetailTransaction> getDetailTransactionListById(String trId){
		List<DetailTransaction> newDetailList = new ArrayList<>();
		
		for(DetailTransaction dt : detailTransactionList) {
			if(dt.getHeaderTransactionId().equals(trId) ) {
				newDetailList.add(dt);
			}
		}
		
		return newDetailList;
	}
	
	public List<HeaderTransaction> getTransactionByUserId(String userId) throws SQLException, ClassNotFoundException{
		List<HeaderTransaction> headerTransactions = new ArrayList<>();
		String query = "SELECT * FROM headertransactions where UserID='" + userId + "'";
		
		try(Connection connection = MySQLConnection.createConnection()){
			try(PreparedStatement ps = connection.prepareStatement(query)){
				try(ResultSet rs = ps.executeQuery()){
					while(rs.next()) {
						String transactionId = rs.getString("TransactionID");
						String transactionDate = rs.getString("TransactionDate");
						HeaderTransaction headerTransaction = new HeaderTransaction(transactionId,userId,transactionDate);
						
						headerTransactions.add(headerTransaction);
					}
				}
			}
		}
		return headerTransactions;
	}
	
	public List<DetailTransaction> getAllDetailTransaction() throws SQLException, ClassNotFoundException{
		List<DetailTransaction> detailTransactions = new ArrayList<>();
		String query = "SELECT * FROM detailtransactions dt JOIN beverages b ON dt.BeverageID = b.BeverageID";
		
		try(Connection connection = MySQLConnection.createConnection()){
			try(PreparedStatement ps = connection.prepareStatement(query)){
				try(ResultSet rs = ps.executeQuery()){
					while(rs.next()) {
						String beverageId = rs.getString("BeverageID");
						String transactionId = rs.getString("TransactionID");
						String beverageName = rs.getString("BeverageName");
						String beverageType = rs.getString("BeverageType");
						int price = rs.getInt("BeveragePrice");
						int quantity = rs.getInt("Quantity");
						int subtotal = price*quantity;
						DetailTransaction detailTransaction = new DetailTransaction(quantity,transactionId,beverageId,beverageName,beverageType,price,subtotal);
						
						detailTransactions.add(detailTransaction);
					}
				}
			}
		}
		return detailTransactions;
	}
	
	
//	public List<HeaderTransaction> fetchAll() throws SQLException, ClassNotFoundException{
//		List<HeaderTransaction> headerTransactions = new ArrayList<>();
//		String query = "SELECT * FROM headertransactions";
//		
//		try(Connection connection = MySQLConnection.createConnection()){
//			try(PreparedStatement ps = connection.prepareStatement(query)){
//				try(ResultSet rs = ps.executeQuery()){
//					while(rs.next()) {
//						String userId = rs.getString("UserID");
//						String transactionId = rs.getString("TransactionID");
//						String transactionDate = rs.getString("TransactionDate");
//						HeaderTransaction headerTransaction = new HeaderTransaction(transactionId,userId,transactionDate);
//
//						System.out.println(headerTransaction.getDate() );
//
//						
//						headerTransactions.add(headerTransaction);
//					}
//				}
//			}
//		}
//		return headerTransactions;
//	}

}
