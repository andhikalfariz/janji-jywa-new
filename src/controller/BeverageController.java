package controller;

import utill.MySQLConnection;

import java.util.List;

import model.Beverage;

import java.util.ArrayList;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;

public class BeverageController {
	
	private static BeverageController instance = null;
	private List<Beverage> beverageList = null;
	
	
	private BeverageController(){
		beverageList = new ArrayList<>();
		try {
			beverageList = fetchAll();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static BeverageController getInstance()
    {
        if (instance == null)
            instance = new BeverageController();
 
        return instance;
    }
	
	public void multiBevDecreaseStock(List<String> idList, List<Integer> newStockList) {
		
		try(Connection connection = MySQLConnection.createConnection()){
			Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			
			String query = "SET SQL_SAFE_UPDATES = 0";
			stmt.addBatch(query);
			for(int i=0; i< idList.size(); i++) {
				String updateQuery = String.format("UPDATE beverages SET BeverageStock = '%s' WHERE BeverageID = '%s';", newStockList.get(i), idList.get(i));
				stmt.addBatch(updateQuery);
			}
			
		    stmt.executeBatch();
		    connection.commit();
			
			beverageList = fetchAll();
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}
	

	
	public String addBeverageStock(String beverageId,String beverageStock, int addStock) {
		int newStock = Integer.parseInt(beverageStock)  + addStock ;
		String snewStock = String.valueOf(newStock);
		
		try(Connection connection = MySQLConnection.createConnection()){
			Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			
			String query1 = "SET SQL_SAFE_UPDATES = 0";
			String query2 = String.format("UPDATE beverages SET BeverageStock = '%s' WHERE BeverageID = '%s';", snewStock,beverageId);
			
			stmt.addBatch(query1);
		    stmt.addBatch(query2);
		    
		    stmt.executeBatch();
		    connection.commit();
			
			beverageList = fetchAll();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return snewStock;
	}
	
	public void updateBeverage(String beverageId, String beverageName, String beverageType, String beveragePrice) {
		try(Connection connection = MySQLConnection.createConnection()){
			Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			
			String query1 = "SET SQL_SAFE_UPDATES = 0";
			String query2 = String.format("UPDATE beverages SET BeverageName = '%s', BeverageType = '%s', BeveragePrice = '%s' WHERE BeverageID = '%s';", 
					beverageName,beverageType,beveragePrice,beverageId);
			
			stmt.addBatch(query1);
		    stmt.addBatch(query2);
		    
		    stmt.executeBatch();
		    connection.commit();
			
			beverageList = fetchAll();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void deleteBeverage(String beverageId) {
		
		try(Connection connection = MySQLConnection.createConnection()){
			Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			
			String query1 = "SET SQL_SAFE_UPDATES = 0";
			String query2 = String.format("DELETE FROM beverages WHERE BeverageID='%s'", beverageId);
			
			stmt.addBatch(query1);
		    stmt.addBatch(query2);
		    
		    stmt.executeBatch();
		    connection.commit();
			
			beverageList = fetchAll();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	public void addBeverage(String newbeverageId,String newbeverageName,String newbeverageType,String newbeveragePrice,String newbeverageStock) {
		try(Connection connection = MySQLConnection.createConnection()){
			Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			
			String query1 = "SET SQL_SAFE_UPDATES = 0";
			String query2 = String.format("INSERT INTO beverages (BeverageID, BeverageName, BeverageType, BeveragePrice, BeverageStock) VALUES ('%s', '%s', '%s', '%s', '%s');"
					, newbeverageId, newbeverageName, newbeverageType, newbeveragePrice, newbeverageStock);
			
			stmt.addBatch(query1);
		    stmt.addBatch(query2);
		    
		    stmt.executeBatch();
		    connection.commit();
			
			beverageList = fetchAll();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}

	
	public List<Beverage> getBeverageList(){
		return beverageList;
	}
	
	public String generateBeverageId() {
		List<String> beveragesId;
		String lastID = "BE001";
		
		beveragesId = fetchAllId();
		
		if(beveragesId.size() != 0 ) {
			lastID = beveragesId.get(beveragesId.size()-1);
		}
		
		int newIdNumber = Integer.parseInt(lastID.substring(2)) + 1;
		
		return String.format("BE%03d" , newIdNumber);
	}

	public List<String> fetchAllId(){
		List<String> beveragesId = new ArrayList<>();
		String query = "SELECT BeverageID FROM beverages";
		
		try(Connection connection = MySQLConnection.createConnection()){
			try(PreparedStatement ps = connection.prepareStatement(query)){
				try(ResultSet rs = ps.executeQuery()){
					while(rs.next()) {
						String id = rs.getString("BeverageID");
						beveragesId.add(id);
					}
				}
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return beveragesId;
	}

	
	public List<Beverage> fetchAll() throws SQLException, ClassNotFoundException{
		List<Beverage> beverages = new ArrayList<>();
		String query = "SELECT * FROM beverages";
		
		try(Connection connection = MySQLConnection.createConnection()){
			try(PreparedStatement ps = connection.prepareStatement(query)){
				try(ResultSet rs = ps.executeQuery()){
					while(rs.next()) {
						String id = rs.getString("BeverageID");
						String name = rs.getString("BeverageName");
						String type = rs.getString("BeverageType");
						int price = rs.getInt("BeveragePrice");
						int stock = rs.getInt("BeverageStock");
						Beverage beverage = new Beverage(id,name,type,price,stock);
							
						beverages.add(beverage);
					}
				}
			}
		}
		return beverages;
	}
	

}
