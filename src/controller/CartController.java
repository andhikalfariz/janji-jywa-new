package controller;

import utill.MySQLConnection;

import java.util.List;


import model.Cart;

import java.util.ArrayList;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import controller.UsersController;
import controller.BeverageController;
import controller.TransactionController;

public class CartController {
	
	private BeverageController beverageController = BeverageController.getInstance();
	private UsersController usersController = UsersController.getInstance();
	private TransactionController transactionController = TransactionController.getInstance();
	private static CartController instance = null;
	private List<Cart> cartList = new ArrayList<>();
	
	private CartController(){}
	
	public static CartController getInstance()
    {
        if (instance == null)
            instance = new CartController();
 
        return instance;
    }
	
	public void checkOutCart() {
		List<String> cartListId = new ArrayList<>();
		List<Integer> cartListStock = new ArrayList<>();
		List<Integer> cartListQuantity = new ArrayList<>();
		
		for(Cart item : cartList) {
			cartListId.add(item.getBeverageId());
			cartListQuantity.add(item.getQuantity());
			cartListStock.add(item.getBeverageStock() - item.getQuantity() );
		}
		
		beverageController.multiBevDecreaseStock(cartListId,cartListStock);
		String transactionID = transactionController.addHeaderTransaction();
		transactionController.addDetailTransaction(transactionID, cartListId, cartListQuantity);
		
		clearCartList();
		
	}
	
	public void clearCartList() {
		cartList.clear();
	}
	
	public int getCartListSize() {
		return cartList.size();
	}
	
	public void removeCartFromList(int idx) {
		cartList.remove(idx);
	}
	
	public void addCartToList(Cart cart) {
		cartList.add(cart);
	}
	
	public int getCartQuantity(int idx) {
		return cartList.get(idx).getQuantity();
	}
	
	public void setCartQuantity(int idx, int newQuantity) {
		cartList.get(idx).setQuantity(newQuantity);
	}
	
	public List<Cart> getCartList(){
		return cartList;
	}
	
	public int checkBeverageInCart( String beverageID ) {
		int idx = 0;
		for(Cart item: cartList) {
			if( beverageID.equals(item.getBeverageId())){
				return idx;
			}
		}
		int notFound = -1000;
		return notFound;
	}

	
//	public List<Cart> fetchAll() throws SQLException, ClassNotFoundException{
//		List<Cart> carts = new ArrayList<>();
//		String query = "SELECT * FROM carts";
//		
//		try(Connection connection = MySQLConnection.createConnection()){
//			try(PreparedStatement ps = connection.prepareStatement(query)){
//				try(ResultSet rs = ps.executeQuery()){
//					while(rs.next()) {
//						String beverageId = rs.getString("BeverageID");
//						String userId = rs.getString("UserID");
//						int quantity = rs.getInt("Quantity");
//						Cart cart = new Cart(userId,beverageId,quantity);
//
////						System.out.println(beverage.getId());
//
//						
//						carts.add(cart);
//					}
//				}
//			}
//		}
//		return carts;
//	}
	
	
}
