package controller;

import utill.MySQLConnection;

import java.util.List;

import model.Beverage;
import model.Users;

import java.util.ArrayList;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class UsersController {
	
	private static UsersController instance = null;
	
	private static Users loggedUser;
	
	private UsersController(){}
	
	public static UsersController getInstance()
    {
        if (instance == null)
            instance = new UsersController();
 
        return instance;
    }
	
	public Users getLoggedUser() {
		return loggedUser;
	}
	
	public static void setLoggedUser(Users user) {
		loggedUser = user;
	}
	
	public void logout() {
		loggedUser = null;
	}
	
	
	
	
	
	
	public void updatePass(Users user , String password) throws SQLException, ClassNotFoundException {
		
		String query = 
				"UPDATE USERS SET UserPassword = \'" + password  + "\' WHERE UserID LIKE \'"+ user.getId() +"\'";
				
//		String query = "INSERT INTO `users` (`UserID`, `UserName`, `UserEmail`, `UserPassword`, `UserDOB`, `UserGender`, `UserAddress`, `UserPhone`, `UserRole`) VALUES ('US004', 'Mijagung', 'admin', 'admin', NULL, 'Male', 'asdasdasdasd Street', '0920398193812319', 'Admin')";
//		try(Connection connection = MySQLConnection.createConnection()){
//			PreparedStatement ps = connection.prepareStatement(query);
//			ps.executeQuery();	
//		}
				
		try(Connection connection = MySQLConnection.createConnection()){
			Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			stmt.addBatch(query);
			stmt.executeBatch();
		    connection.commit();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
}
	
	
	
	
	
	
	
	public void update(Users user , String username, String email, String phone, String address, String gender) throws SQLException, ClassNotFoundException {
		
			String query = 
					"UPDATE USERS SET UserName = \'"+ username + "\', UserEmail = \'"+ email + "\' , UserPhone = \'" + phone + "\', UserAddress = \'" + address + "\' ,UserGender = \'" + gender + "\'" + " Where UserID LIKE \'" + user.getId() + "\'";
					
//			String query = "INSERT INTO `users` (`UserID`, `UserName`, `UserEmail`, `UserPassword`, `UserDOB`, `UserGender`, `UserAddress`, `UserPhone`, `UserRole`) VALUES ('US004', 'Mijagung', 'admin', 'admin', NULL, 'Male', 'asdasdasdasd Street', '0920398193812319', 'Admin')";
//			try(Connection connection = MySQLConnection.createConnection()){
//				PreparedStatement ps = connection.prepareStatement(query);
//				ps.executeQuery();	
//			}
					
			try(Connection connection = MySQLConnection.createConnection()){
				Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
				stmt.addBatch(query);
				stmt.executeBatch();
			    connection.commit();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
	}
	
	
	
	
	
	
	
	
	
	public static Users getUserByEmail(String inputEmail) throws SQLException, ClassNotFoundException {
		Users user;
		String query = "SELECT * FROM Users WHERE UserEmail LIKE \'" + inputEmail + "\'";
		try(Connection connection = MySQLConnection.createConnection()){
			try(PreparedStatement ps = connection.prepareStatement(query)){
				try(ResultSet rs = ps.executeQuery()){
					System.out.println(rs);
					while(rs.next()) {
						String id = rs.getString("UserID");
						String name = rs.getString("UserName");
						String email = rs.getString("UserEmail");
						String password = rs.getString("UserPassword");
						String gender = rs.getString("UserGender");
						String address = rs.getString("UserAddress");
						String phone = rs.getString("UserPhone");
						String role = rs.getString("UserRole");
						user = new Users(id,name,email,password,gender,address,phone,role);
						return user;
					}
				}
			}
		}
		return null;
	}
	
	public static void register(String id, String username, String email, String phone, String address, 
			String password, String gender, String role) throws SQLException, ClassNotFoundException {
			String query = "INSERT INTO Users (UserID, UserName, UserEmail, UserPassword, UserGender, UserAddress, UserPhone, UserRole) VALUES (\'" + id + "\',\'" + username + "\',\'" + email + "\',\'" + password + "\',\'" + gender + "\',\'" + address + "\',\'" + phone + "\',\'" + role + "\')";

			try(Connection connection = MySQLConnection.createConnection()){
				Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
				stmt.addBatch(query);
				stmt.executeBatch();
			    connection.commit();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
	}
	
	
	public static String generateId() {
		String id;
		String lastID;
		String newId;
		String lastIdStr;
		int checkCurrent;
		int lastIdInt;
		
		List<String> listId = new ArrayList<>();
		
		try {
			listId = instance.fetchId();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		lastID = listId.get(listId.size()-1);

		checkCurrent = Integer.parseInt(lastID.substring(2));
		System.out.println(checkCurrent/10);
		
		if (checkCurrent/10 < 1 && checkCurrent!=9) {
			System.out.println("masuk 1");
			lastIdInt = Character.getNumericValue(lastID.charAt(4)) + 1;
	        lastIdStr = Integer.toString(lastIdInt);	        
	        id = lastID.substring(0,4);
	        newId = id + lastIdStr;
	        return newId;
		
		} else if (checkCurrent/10 < 10 && checkCurrent!=99) {
			System.out.println("masuk 2");
			lastIdInt = Integer.parseInt(lastID.substring(3)) + 1;
	        lastIdStr = Integer.toString(lastIdInt);	        
	        id = lastID.substring(0,3);
	        newId = id + lastIdStr;
	        return newId;
		
		} else if (checkCurrent/10 < 100 && checkCurrent!=999) {
			System.out.println("masuk 3");
			lastIdInt = Integer.parseInt(lastID.substring(2)) + 1;
	        lastIdStr = Integer.toString(lastIdInt);
	        id = lastID.substring(0,2);
	        newId = id + lastIdStr;
	        return newId;
			
		}
		return null;
        
	}
	
	
	
	public static List<String> fetchId() throws SQLException, ClassNotFoundException{
		List<String> listUsersId = new ArrayList<>();
		String query = "SELECT UserID FROM users";
		
		try(Connection connection = MySQLConnection.createConnection()){
			try(PreparedStatement ps = connection.prepareStatement(query)){
				try(ResultSet rs = ps.executeQuery()){
					while(rs.next()) {
						listUsersId.add(rs.getString("UserID"));
					}
				}
			}
		}
		return listUsersId;
	}
	
	public List<Users> fetchAll() throws SQLException, ClassNotFoundException{
		List<Users> listUsers = new ArrayList<>();
		String query = "SELECT * FROM users";
		
		try(Connection connection = MySQLConnection.createConnection()){
			try(PreparedStatement ps = connection.prepareStatement(query)){
				try(ResultSet rs = ps.executeQuery()){
					while(rs.next()) {
						String id = rs.getString("UserID");
						String name = rs.getString("UserName");
						String email = rs.getString("UserEmail");
						String password = rs.getString("UserPassword");
						String gender = rs.getString("UserGender");
						String address = rs.getString("UserAddress");
						String phone = rs.getString("UserPhone");
						String role = rs.getString("UserRole");
						Users users = new Users(id,name,email,password,gender,address,phone,role);

						
						listUsers.add(users);
					}
				}
			}
		}
		return listUsers;
	}
}

