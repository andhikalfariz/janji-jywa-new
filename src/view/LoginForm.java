package view;

import java.awt.EventQueue;

import java.awt.*;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;

import controller.UsersController;
import model.Users;

public class LoginForm {

	private JFrame frmLoginForm;
	private JTextField emailField;
	private JPasswordField passwordField;
	static LoginForm window;
	static boolean check = false;
	
	public static LoginForm login() {
		return window;
	}

	public void setVisiblilogin(LoginForm windows) {
		windows = new LoginForm();
		windows.frmLoginForm.setVisible(true);
	}

	private static void btnOptionDialogActionPerformed(java.awt.event.ActionEvent evt, JTextField emailField,
			JTextField passwordField) {

		if ((emailField.getText().equals("")) || (passwordField.getText().equals(""))) {

			JOptionPane.showMessageDialog(null, "Fill all requirements!");

		} else {
			Users checkUser;
			try {
				checkUser = UsersController.getUserByEmail(emailField.getText());
				if (checkUser != null) {
					if (checkUser.getPassword().equals(passwordField.getText())) {
						UsersController.setLoggedUser(checkUser);
						MainForm2 mainForm = new MainForm2();
						mainForm.setVisibliMain(mainForm.window);
						check = true;
						window.frmLoginForm.dispose();
						window.frmLoginForm.setVisible(false);
//						TransactionForm trans = new TransactionForm();

					} else {
						JOptionPane.showMessageDialog(null, "Wrong email/password!");
					}

				} else {
					JOptionPane.showMessageDialog(null, "Wrong email/password!");
				}

			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	};

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					window = new LoginForm();
					window.frmLoginForm.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LoginForm() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmLoginForm = new JFrame();
		frmLoginForm.setTitle("Login Form");
		frmLoginForm.getContentPane().setBackground(Color.CYAN);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		frmLoginForm.getContentPane().setLayout(gridBagLayout);

		JLabel loginFormLabel = new JLabel("Login Form");
		GridBagConstraints gbc_loginFormLabel = new GridBagConstraints();
		gbc_loginFormLabel.gridwidth = 3;
		gbc_loginFormLabel.anchor = GridBagConstraints.WEST;
		gbc_loginFormLabel.insets = new Insets(0, 0, 5, 0);
		gbc_loginFormLabel.gridx = 5;
		gbc_loginFormLabel.gridy = 0;
		frmLoginForm.getContentPane().add(loginFormLabel, gbc_loginFormLabel);

		JLabel emailLabel = new JLabel(" Email");
		GridBagConstraints gbc_emailLabel = new GridBagConstraints();
		gbc_emailLabel.gridwidth = 3;
		gbc_emailLabel.anchor = GridBagConstraints.WEST;
		gbc_emailLabel.insets = new Insets(0, 0, 5, 5);
		gbc_emailLabel.gridx = 0;
		gbc_emailLabel.gridy = 2;
		frmLoginForm.getContentPane().add(emailLabel, gbc_emailLabel);

		emailField = new JTextField();
		GridBagConstraints gbc_emailField = new GridBagConstraints();
		gbc_emailField.insets = new Insets(0, 0, 5, 0);
		gbc_emailField.fill = GridBagConstraints.HORIZONTAL;
		gbc_emailField.gridx = 5;
		gbc_emailField.gridy = 2;
		frmLoginForm.getContentPane().add(emailField, gbc_emailField);
		emailField.setColumns(10);

		JLabel passwordLabel = new JLabel("Password");
		GridBagConstraints gbc_passwordLabel = new GridBagConstraints();
		gbc_passwordLabel.gridwidth = 3;
		gbc_passwordLabel.insets = new Insets(0, 0, 5, 5);
		gbc_passwordLabel.gridx = 0;
		gbc_passwordLabel.gridy = 3;
		frmLoginForm.getContentPane().add(passwordLabel, gbc_passwordLabel);

		passwordField = new JPasswordField();
		GridBagConstraints gbc_passwordField = new GridBagConstraints();
		gbc_passwordField.insets = new Insets(0, 0, 5, 0);
		gbc_passwordField.fill = GridBagConstraints.HORIZONTAL;
		gbc_passwordField.gridx = 5;
		gbc_passwordField.gridy = 3;
		frmLoginForm.getContentPane().add(passwordField, gbc_passwordField);

		JButton loginButton = new JButton("Login");
		GridBagConstraints gbc_loginButton = new GridBagConstraints();
		gbc_loginButton.insets = new Insets(0, 0, 5, 0);
		gbc_loginButton.anchor = GridBagConstraints.WEST;
		gbc_loginButton.gridx = 5;
		gbc_loginButton.gridy = 5;
		frmLoginForm.getContentPane().add(loginButton, gbc_loginButton);

		loginButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnOptionDialogActionPerformed(evt, emailField, passwordField);
				
				if(check == true) {
					frmLoginForm.dispose();
				}
			}
		});

		GridBagConstraints gbc_signUpHereLabel = new GridBagConstraints();
		JLabel signUpHereLabel = new JLabel("Sign Up Here");
		gbc_signUpHereLabel.anchor = GridBagConstraints.WEST;
		gbc_signUpHereLabel.gridx = 5;
		gbc_signUpHereLabel.gridy = 7;
		frmLoginForm.getContentPane().add(signUpHereLabel, gbc_signUpHereLabel);

//		signUpHereLabel = new JLabel("Sign In");
		signUpHereLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {
				RegisterForm regis = new RegisterForm();
				regis.setVisible(true);
				frmLoginForm.dispose();
			}
		});

		frmLoginForm.setBounds(100, 100, 450, 300);
		frmLoginForm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		System.out.println("Hellow");
	}
}