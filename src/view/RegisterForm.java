package view;

import javax.swing.border.EmptyBorder;

import controller.UsersController;
import model.Users;

import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Enumeration;
import java.awt.event.ActionEvent;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class RegisterForm extends JFrame {
	
	private JFrame frame = new JFrame();
	private JPanel contentPane;
	private JTextField idField;
	private JLabel lblNewLabel_2;
	private JLabel lblNewLabel_3;
	private JLabel lblNewLabel_4;
	private JLabel lblNewLabel_5;
	private JLabel lblNewLabel_6;
	private JLabel lblNewLabel_7;
	private JLabel lblNewLabel_8;
	private JTextField usernameField;
	private JTextField emailField;
	private JTextField phoneField;
	private JTextField addressField;
	private JPasswordField passwordField;
	private ButtonGroup G1;
	private JRadioButton femaleRadioButton;
	private JRadioButton maleRadioButton;
	private JComboBox roleComboBox;
	private JLabel signInLabel;
	static boolean check = false;
//	static RegisterForm frames;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegisterForm frames = new RegisterForm();
					frames.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RegisterForm() {
		setTitle("Register Form");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 495, 541);
//		G1.add(rdbtnNewRadioButton);
//		G1.add(rdbtnNewRadioButton_1);
		contentPane = new JPanel();
		contentPane.setBackground(Color.CYAN);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		JLabel lblNewLabel = new JLabel("Register Form");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 5;
		gbc_lblNewLabel.gridy = 0;
		contentPane.add(lblNewLabel, gbc_lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Id");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 2;
		contentPane.add(lblNewLabel_1, gbc_lblNewLabel_1);

		idField = new JTextField();
		String idNew = UsersController.generateId();
		idField.setText(idNew);
		idField.setEditable(false);
		GridBagConstraints gbc_idField = new GridBagConstraints();
		gbc_idField.gridwidth = 4;
		gbc_idField.fill = GridBagConstraints.HORIZONTAL;
		gbc_idField.insets = new Insets(0, 0, 5, 0);
		gbc_idField.gridx = 5;
		gbc_idField.gridy = 2;
		contentPane.add(idField, gbc_idField);
		idField.setColumns(10);

		lblNewLabel_2 = new JLabel("User Name");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 0;
		gbc_lblNewLabel_2.gridy = 3;
		contentPane.add(lblNewLabel_2, gbc_lblNewLabel_2);

		usernameField = new JTextField();
		GridBagConstraints gbc_usernameField = new GridBagConstraints();
		gbc_usernameField.gridwidth = 4;
		gbc_usernameField.insets = new Insets(0, 0, 5, 0);
		gbc_usernameField.fill = GridBagConstraints.HORIZONTAL;
		gbc_usernameField.gridx = 5;
		gbc_usernameField.gridy = 3;
		contentPane.add(usernameField, gbc_usernameField);
		usernameField.setColumns(10);

		lblNewLabel_3 = new JLabel("Email");
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_3.gridx = 0;
		gbc_lblNewLabel_3.gridy = 4;
		contentPane.add(lblNewLabel_3, gbc_lblNewLabel_3);

		emailField = new JTextField();
		GridBagConstraints gbc_emailField = new GridBagConstraints();
		gbc_emailField.gridwidth = 4;
		gbc_emailField.insets = new Insets(0, 0, 5, 0);
		gbc_emailField.fill = GridBagConstraints.HORIZONTAL;
		gbc_emailField.gridx = 5;
		gbc_emailField.gridy = 4;
		contentPane.add(emailField, gbc_emailField);
		emailField.setColumns(10);

		lblNewLabel_4 = new JLabel("Phone");
		lblNewLabel_4.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblNewLabel_4 = new GridBagConstraints();
		gbc_lblNewLabel_4.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_4.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_4.gridx = 0;
		gbc_lblNewLabel_4.gridy = 5;
		contentPane.add(lblNewLabel_4, gbc_lblNewLabel_4);

		phoneField = new JTextField();
		GridBagConstraints gbc_phoneField = new GridBagConstraints();
		gbc_phoneField.gridwidth = 4;
		gbc_phoneField.insets = new Insets(0, 0, 5, 0);
		gbc_phoneField.fill = GridBagConstraints.HORIZONTAL;
		gbc_phoneField.gridx = 5;
		gbc_phoneField.gridy = 5;
		contentPane.add(phoneField, gbc_phoneField);
		phoneField.setColumns(10);

		lblNewLabel_5 = new JLabel("Address");
		lblNewLabel_5.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblNewLabel_5 = new GridBagConstraints();
		gbc_lblNewLabel_5.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_5.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_5.gridx = 0;
		gbc_lblNewLabel_5.gridy = 6;
		contentPane.add(lblNewLabel_5, gbc_lblNewLabel_5);

		addressField = new JTextField();
		GridBagConstraints gbc_addressField = new GridBagConstraints();
		gbc_addressField.gridwidth = 4;
		gbc_addressField.insets = new Insets(0, 0, 5, 0);
		gbc_addressField.fill = GridBagConstraints.HORIZONTAL;
		gbc_addressField.gridx = 5;
		gbc_addressField.gridy = 6;
		contentPane.add(addressField, gbc_addressField);
		addressField.setColumns(10);

		lblNewLabel_6 = new JLabel("Password");
		lblNewLabel_6.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblNewLabel_6 = new GridBagConstraints();
		gbc_lblNewLabel_6.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_6.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_6.gridx = 0;
		gbc_lblNewLabel_6.gridy = 7;
		contentPane.add(lblNewLabel_6, gbc_lblNewLabel_6);

		passwordField = new JPasswordField();
		GridBagConstraints gbc_passwordField = new GridBagConstraints();
		gbc_passwordField.gridwidth = 4;
		gbc_passwordField.insets = new Insets(0, 0, 5, 0);
		gbc_passwordField.fill = GridBagConstraints.HORIZONTAL;
		gbc_passwordField.gridx = 5;
		gbc_passwordField.gridy = 7;
		contentPane.add(passwordField, gbc_passwordField);

		lblNewLabel_7 = new JLabel("Gender");
		lblNewLabel_7.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblNewLabel_7 = new GridBagConstraints();
		gbc_lblNewLabel_7.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_7.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_7.gridx = 0;
		gbc_lblNewLabel_7.gridy = 8;
		contentPane.add(lblNewLabel_7, gbc_lblNewLabel_7);

		femaleRadioButton = new JRadioButton("Female");
		femaleRadioButton.setBackground(Color.CYAN);
		femaleRadioButton.setSelected(true);
		GridBagConstraints gbc_femaleRadioButton = new GridBagConstraints();
		gbc_femaleRadioButton.insets = new Insets(0, 0, 5, 5);
		gbc_femaleRadioButton.gridx = 5;
		gbc_femaleRadioButton.gridy = 8;
		contentPane.add(femaleRadioButton, gbc_femaleRadioButton);

		maleRadioButton = new JRadioButton("Male");
		maleRadioButton.setBackground(Color.CYAN);
		GridBagConstraints gbc_maleRadioButton = new GridBagConstraints();
		gbc_maleRadioButton.insets = new Insets(0, 0, 5, 0);
		gbc_maleRadioButton.gridx = 8;
		gbc_maleRadioButton.gridy = 8;
		contentPane.add(maleRadioButton, gbc_maleRadioButton);

		ButtonGroup genderButtonGroup = new ButtonGroup();
		genderButtonGroup.add(femaleRadioButton);
		genderButtonGroup.add(maleRadioButton);

		lblNewLabel_8 = new JLabel("Role");
		lblNewLabel_8.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblNewLabel_8 = new GridBagConstraints();
		gbc_lblNewLabel_8.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_8.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_8.gridx = 0;
		gbc_lblNewLabel_8.gridy = 9;
		contentPane.add(lblNewLabel_8, gbc_lblNewLabel_8);

		String roles[] = { "Admin", "Customer" };
		roleComboBox = new JComboBox(roles);
		GridBagConstraints gbc_roleComboBox = new GridBagConstraints();
		gbc_roleComboBox.gridwidth = 3;
		gbc_roleComboBox.insets = new Insets(0, 0, 5, 5);
		gbc_roleComboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_roleComboBox.gridx = 5;
		gbc_roleComboBox.gridy = 9;
		contentPane.add(roleComboBox, gbc_roleComboBox);

		
//		JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
		
		
		JButton registerButton = new JButton("Register");
		registerButton.addActionListener(new java.awt.event.ActionListener() {

			
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnOptionDialogActionPerformed(evt,idField, usernameField, emailField, phoneField, addressField,
						passwordField, genderButtonGroup, roleComboBox);
				
				if(check == true) {
					setVisible(false);
				}
						
			}
		});

		GridBagConstraints gbc_registerButton = new GridBagConstraints();
		gbc_registerButton.gridwidth = 2;
		gbc_registerButton.insets = new Insets(0, 0, 5, 5);
		gbc_registerButton.gridx = 4;
		gbc_registerButton.gridy = 11;
		contentPane.add(registerButton, gbc_registerButton);

		signInLabel = new JLabel("Sign In");
		signInLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {
				LoginForm login = new LoginForm();
				login.setVisiblilogin(login);
				setVisible(false);
				
				
//				frames.dispose();
				
//				frame.setVisible(false);
			}
		});

		GridBagConstraints gbc_signInLabel = new GridBagConstraints();
		gbc_signInLabel.gridwidth = 2;
		gbc_signInLabel.insets = new Insets(0, 0, 0, 5);
		gbc_signInLabel.gridx = 4;
		gbc_signInLabel.gridy = 12;
		contentPane.add(signInLabel, gbc_signInLabel);
		
		
		

	}

	private static void btnOptionDialogActionPerformed(ActionEvent evt, JTextField idField, JTextField usernameField,
			JTextField emailField, JTextField phoneField, JTextField addressField, JPasswordField passwordField,
			ButtonGroup genderButtonGroup, JComboBox roleComboBox) {
		// TODO Auto-generated method stub
		if ((idField.getText().equals("")) || (usernameField.getText().equals("")) || (emailField.getText().equals(""))
				|| (emailField.getText().equals("")) || (phoneField.getText().equals(""))
				|| (addressField.getText().equals("")) || (passwordField.getText().equals(""))) {

			JOptionPane.showMessageDialog(null, "Fill all requirements!");

		} else {
			if (usernameField.getText().length() < 5 || usernameField.getText().length() > 30) {
				JOptionPane.showMessageDialog(null, "Username must be between 5 - 30 characters!");
//				RegisterForm frame = new RegisterForm();
//				frame.setVisible(true);

			} else {
				if (emailIsValid(emailField.getText()) == false) {
					JOptionPane.showMessageDialog(null, "Email must be in valid format!");
//					RegisterForm frame = new RegisterForm();
//					frame.setVisible(true);

				} else {
					if (phoneField.getText().length() < 12
							|| allDigits(phoneField.getText(), phoneField.getText().length()) == false) {
						JOptionPane.showMessageDialog(null,
								"Phone number must be numeric and more than equals 12 digits");
//						RegisterForm frame = new RegisterForm();
//						frame.setVisible(true);

					} else {
						if (isValidAddress(addressField.getText()) == false || addressField.getText().length() < 10) {
							JOptionPane.showMessageDialog(null,
									"Address must consist of 10 or more characters and ends with � Street�");
//							RegisterForm frame = new RegisterForm();
//							frame.setVisible(true);

						} else {
							if (isAlphanumeric(passwordField.getText()) == false || passwordField.getText().length() < 5
									|| passwordField.getText().length() > 30) {
								JOptionPane.showMessageDialog(null,
										"Password must at least contain 1 character Password must 5 - 30 length of character and digit and 1 digit!");
								

							} else {
								try {
									UsersController.register(idField.getText(), usernameField.getText(),
											emailField.getText(), phoneField.getText(), addressField.getText(),
											passwordField.getText(), getSelectedButtonText(genderButtonGroup),
											(String) roleComboBox.getSelectedItem());
									JOptionPane.showMessageDialog(null,
											"Succesfully Register!");
									LoginForm login = new LoginForm();
									login.setVisiblilogin(login);
									check = true;
									
									
									
								} catch (ClassNotFoundException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (SQLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}
					}
				}
			}
		}
	}

	// Code taken from GeeksForGeeks
	public static boolean emailIsValid(String email) {
		String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." + "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z"
				+ "A-Z]{2,7}$";

		Pattern pat = Pattern.compile(emailRegex);
		if (email == null)
			return false;
		return pat.matcher(email).matches();
	}

	// Code Taken From GeeksForGeeks
	static int MAX = 10;

	static boolean isDigit(char ch) {
		if (ch >= '0' && ch <= '9')
			return true;
		return false;
	}

	// Function that returns true
	// if str contains all the
	// digits from 0 to 9
	static boolean allDigits(String str, int len) {
		for (int i = 0; i < len; i++) {
			if (isDigit(str.charAt(i)) == false) {
				return false;
			}
		}
		return true;
	}

	public static boolean isAlphanumeric(String str) {
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (!Character.isLetterOrDigit(c))
				return false;
		}
		return true;
	}

	public static boolean isValidAddress(String address) {
		String[] listWords = address.split(" ");
		String lastWord = listWords[listWords.length - 1];
		if (lastWord.equalsIgnoreCase("street")) {
			return true;
		} else {
			return false;
		}
	}

	public static String getSelectedButtonText(ButtonGroup buttonGroup) {
		for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements();) {
			AbstractButton button = buttons.nextElement();

			if (button.isSelected()) {
				return button.getText();
			}
		}

		return null;
	}

}
