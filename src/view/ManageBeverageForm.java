package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Color;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JSpinner;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import model.Beverage;
import controller.BeverageController;
import controller.UsersController;

import javax.swing.SpinnerNumberModel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class ManageBeverageForm {

	private JFrame frmManageBeverages;
	private JTable manageBeverageTable;
	private JLabel newBeverageNameLabel;
	private JLabel newBeverageTypeLabel;
	private JLabel newBeveragePriceLabel;
	private JTextField newBeverageIdField;
	private JTextField newBeverageNameField;
	private JTextField newBeveragePriceField;
	private JTextField beverageStockField;
	private JTextField beveragePriceField;
	private JTextField beverageIdField;
	private JTextField beverageNameField;
	private JLabel beverageStockLabel;
	private JLabel beveragePriceLabel;
	private JLabel beverageTypeLabel;
	private JLabel beverageNameLabel;
	private JLabel beverageIdLabel;

	private BeverageController beverageController;
	private String newBevId;
	static ManageBeverageForm window;
	static UsersController userController = UsersController.getInstance();
	/**
	 * Launch the application.
	 */
	
	
	
	public static ManageBeverageForm login(){
		return window;
	};
	
	public void setVisibliManage(ManageBeverageForm windows) {
		windows = new ManageBeverageForm();
		windows.frmManageBeverages.setVisible(true);
	}
	
	
	
	private static void btnOptionDialogActionPerformed(java.awt.event.ActionEvent evt) {

        int jawab = JOptionPane.showOptionDialog(null,
                "You sure want to exit?",
                "Warning",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE, null, null, null);

        if (jawab == JOptionPane.YES_OPTION) {
            JOptionPane.showMessageDialog(null, "Program exit");
            System.exit(0);
        }
	};
	
	private static void btnOptionDialogActionPerformed2(java.awt.event.ActionEvent evt) {
		JOptionPane.showMessageDialog(null, "You'll Log Off");
		userController.logout();
		LoginForm login = new LoginForm();
		login.setVisiblilogin(login.window);
		
//        int jawab = JOptionPane.showOptionDialog(null,
//                "You sure want to Log Off?",
//                "Warning",
//                JOptionPane.YES_NO_OPTION,
//                JOptionPane.QUESTION_MESSAGE, null, null, null);
//
//        if (jawab == JOptionPane.YES_OPTION) {
//            JOptionPane.showMessageDialog(null, "You'll Log Off");
//            userController.logout();
//            LoginForm login = new LoginForm();
//            login.setVisiblilogin(login.window);
////            frame.dispose();
// 
//        }
	};
	
	
	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					window = new ManageBeverageForm();
					window.frmManageBeverages.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ManageBeverageForm() {
		beverageController = BeverageController.getInstance();
		newBevId = beverageController.generateBeverageId();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmManageBeverages = new JFrame();
		frmManageBeverages.setTitle("Manage Beverages");
		frmManageBeverages.getContentPane().setBackground(Color.CYAN);
		frmManageBeverages.setBounds(100, 100, 819, 689);
		frmManageBeverages.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmManageBeverages.getContentPane().setLayout(null);

		JLabel manageBeverageLabel = new JLabel("Manage Beverage");
		manageBeverageLabel.setHorizontalAlignment(SwingConstants.CENTER);
		manageBeverageLabel.setBounds(10, 11, 785, 14);
		frmManageBeverages.getContentPane().add(manageBeverageLabel);

		manageBeverageTable = new JTable();

		BeverageTableModel beverageTableModel = new BeverageTableModel(beverageController.getBeverageList());
		manageBeverageTable.setModel(beverageTableModel);

		manageBeverageTable.setBounds(10, 36, 785, 228);
		frmManageBeverages.getContentPane().add(manageBeverageTable);

		JLabel newBeverageIdLabel = new JLabel("New Beverage ID");
		newBeverageIdLabel.setBounds(10, 275, 115, 14);
		frmManageBeverages.getContentPane().add(newBeverageIdLabel);

		newBeverageNameLabel = new JLabel("New Beverage Name");
		newBeverageNameLabel.setBounds(10, 300, 130, 14);
		frmManageBeverages.getContentPane().add(newBeverageNameLabel);

		newBeverageTypeLabel = new JLabel("New Beverage Type");
		newBeverageTypeLabel.setBounds(10, 325, 115, 14);
		frmManageBeverages.getContentPane().add(newBeverageTypeLabel);

		newBeveragePriceLabel = new JLabel("New Beverage Price");
		newBeveragePriceLabel.setBounds(10, 350, 115, 14);
		frmManageBeverages.getContentPane().add(newBeveragePriceLabel);

		JLabel newBeverageStockLabel = new JLabel("New Beverage Stock");
		newBeverageStockLabel.setBounds(10, 375, 142, 14);
		frmManageBeverages.getContentPane().add(newBeverageStockLabel);

		newBeverageIdField = new JTextField();
		newBeverageIdField.setEditable(false);
		newBeverageIdField.setText(newBevId);
		newBeverageIdField.setBounds(162, 272, 205, 20);
		frmManageBeverages.getContentPane().add(newBeverageIdField);
		newBeverageIdField.setColumns(10);

		newBeverageNameField = new JTextField();
		newBeverageNameField.setColumns(10);
		newBeverageNameField.setBounds(162, 297, 205, 20);
		frmManageBeverages.getContentPane().add(newBeverageNameField);

		newBeveragePriceField = new JTextField();
		newBeveragePriceField.setColumns(10);
		newBeveragePriceField.setBounds(162, 347, 205, 20);
		frmManageBeverages.getContentPane().add(newBeveragePriceField);

		beverageStockField = new JTextField();
		beverageStockField.setEditable(false);
		beverageStockField.setColumns(10);
		beverageStockField.setBounds(590, 372, 205, 20);
		frmManageBeverages.getContentPane().add(beverageStockField);

		beveragePriceField = new JTextField();
		beveragePriceField.setColumns(10);
		beveragePriceField.setBounds(590, 347, 205, 20);
		frmManageBeverages.getContentPane().add(beveragePriceField);

		beverageIdField = new JTextField();
		beverageIdField.setEditable(false);
		beverageIdField.setColumns(10);
		beverageIdField.setBounds(590, 272, 205, 20);
		frmManageBeverages.getContentPane().add(beverageIdField);

		beverageNameField = new JTextField();
		beverageNameField.setColumns(10);
		beverageNameField.setBounds(590, 297, 205, 20);
		frmManageBeverages.getContentPane().add(beverageNameField);

		beverageStockLabel = new JLabel("Beverage Stock");
		beverageStockLabel.setBounds(405, 375, 101, 14);
		frmManageBeverages.getContentPane().add(beverageStockLabel);

		beveragePriceLabel = new JLabel("Beverage Price");
		beveragePriceLabel.setBounds(405, 350, 115, 14);
		frmManageBeverages.getContentPane().add(beveragePriceLabel);

		beverageTypeLabel = new JLabel("Beverage Type");
		beverageTypeLabel.setBounds(405, 325, 115, 14);
		frmManageBeverages.getContentPane().add(beverageTypeLabel);

		beverageNameLabel = new JLabel("Beverage Name");
		beverageNameLabel.setBounds(405, 300, 130, 14);
		frmManageBeverages.getContentPane().add(beverageNameLabel);

		beverageIdLabel = new JLabel("Beverage ID");
		beverageIdLabel.setBounds(405, 275, 115, 14);
		frmManageBeverages.getContentPane().add(beverageIdLabel);

		JButton deleteBeverageButton = new JButton("Delete Beverage");
		deleteBeverageButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!beverageIdField.getText().equals("")) {

					int jawab = JOptionPane.showOptionDialog(null, "Are you sure you want to delete beverage?",
							"Confirmation Message", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null,
							null);

					if (jawab == JOptionPane.YES_OPTION) {
						String bevID = beverageIdField.getText();
						beverageController.deleteBeverage(bevID);

						beverageIdField.setText("");
						beverageNameField.setText("");
						beveragePriceField.setText("");
						beverageStockField.setText("");

						BeverageTableModel beverageTableModel = new BeverageTableModel(
								beverageController.getBeverageList());
						manageBeverageTable.setModel(beverageTableModel);
					}
				} else {
					JOptionPane.showMessageDialog(null, "Please fill the beverage id!");
				}

			}
		});
		deleteBeverageButton.setBounds(611, 420, 184, 23);
		frmManageBeverages.getContentPane().add(deleteBeverageButton);

		JComboBox newBeverageTypeComboBox = new JComboBox();
		newBeverageTypeComboBox
				.setModel(new DefaultComboBoxModel(new String[] { "Boba", "Coffee", "Tea", "Smoothies" }));
		newBeverageTypeComboBox.setBounds(162, 321, 205, 22);
		frmManageBeverages.getContentPane().add(newBeverageTypeComboBox);

		JComboBox beverageTypeComboBox = new JComboBox();
		beverageTypeComboBox.setModel(new DefaultComboBoxModel(new String[] { "Boba", "Coffee", "Tea", "Smoothies" }));
		beverageTypeComboBox.setBounds(590, 321, 205, 22);
		frmManageBeverages.getContentPane().add(beverageTypeComboBox);

		JButton updateBeverageButton = new JButton("Update Beverage");
		updateBeverageButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!beverageIdField.getText().equals("")) {
					String beverageId = beverageIdField.getText();
					String beverageName = beverageNameField.getText();
					String beverageType = (String) beverageTypeComboBox.getSelectedItem();
					String beveragePrice = beveragePriceField.getText();

					if (beverageUpdateValidate(beverageName, beverageType, beveragePrice)) {

						int jawab = JOptionPane.showOptionDialog(null, "Are you sure you want to update beverage?",
								"Confirmation Message", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,
								null, null);

						if (jawab == JOptionPane.YES_OPTION) {

							beverageController.updateBeverage(beverageId, beverageName, beverageType, beveragePrice);

							BeverageTableModel beverageTableModel = new BeverageTableModel(
									beverageController.getBeverageList());
							manageBeverageTable.setModel(beverageTableModel);

						}

					} else {
						JOptionPane.showMessageDialog(null, "Invalid beverage data!");
					}
				} else {
					JOptionPane.showMessageDialog(null, "Please fill the beverage id!");
				}
			}
		});
		updateBeverageButton.setBounds(405, 420, 184, 23);
		frmManageBeverages.getContentPane().add(updateBeverageButton);

		JLabel addStockLabel = new JLabel("Add Stock");
		addStockLabel.setBounds(405, 458, 59, 14);
		frmManageBeverages.getContentPane().add(addStockLabel);

		JSpinner addStockField = new JSpinner();
		addStockField.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		addStockField.setBounds(488, 455, 139, 20);
		frmManageBeverages.getContentPane().add(addStockField);

		JButton addStockButton = new JButton("Add Stock");
		addStockButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!beverageIdField.getText().equals("")) {
					String beverageId = beverageIdField.getText();
					String beverageStock = beverageStockField.getText();
					int addStock = (Integer) addStockField.getValue();

					if (addStockValidate(addStock)) {

						int jawab = JOptionPane.showOptionDialog(null, "Are you sure you want to add beverage stock?",
								"Confirmation Message", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,
								null, null);

						if (jawab == JOptionPane.YES_OPTION) {

							String newStock = beverageController.addBeverageStock(beverageId, beverageStock, addStock);

							BeverageTableModel beverageTableModel = new BeverageTableModel(
									beverageController.getBeverageList());
							manageBeverageTable.setModel(beverageTableModel);

							beverageStockField.setText(newStock);

						}

					} else {
						JOptionPane.showMessageDialog(null, "Invalid beverage data!");
					}
				} else {
					JOptionPane.showMessageDialog(null, "Please fill the beverage id!");
				}
			}
		});
		addStockButton.setBounds(637, 454, 158, 23);
		frmManageBeverages.getContentPane().add(addStockButton);

		manageBeverageTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int i = manageBeverageTable.getSelectedRow();
				beverageIdField.setText(manageBeverageTable.getValueAt(i, 0).toString());
				beverageNameField.setText(manageBeverageTable.getValueAt(i, 1).toString());
				beverageTypeComboBox.setSelectedItem(manageBeverageTable.getValueAt(i, 2).toString());
				beveragePriceField.setText(manageBeverageTable.getValueAt(i, 3).toString());
				beverageStockField.setText(manageBeverageTable.getValueAt(i, 4).toString());
			}
		});

		JSpinner newBeverageStockField = new JSpinner();
		newBeverageStockField.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		newBeverageStockField.setBounds(162, 372, 205, 20);
		frmManageBeverages.getContentPane().add(newBeverageStockField);

		JButton insertBeverageButton = new JButton("Insert Beverage");
		insertBeverageButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!newBeverageNameField.getText().equals("") && !newBeveragePriceField.getText().equals("")) {
					String newbeverageId = newBeverageIdField.getText();
					String newbeverageName = newBeverageNameField.getText();
					String newbeverageType = (String) newBeverageTypeComboBox.getSelectedItem();
					String newbeveragePrice = newBeveragePriceField.getText();
					int newbeverageStock = (Integer) newBeverageStockField.getValue();

					if (addStockValidate(newbeverageStock)
							&& beverageUpdateValidate(newbeverageName, newbeverageType, newbeveragePrice)) {

						int jawab = JOptionPane.showOptionDialog(null, "Are you sure you want to insert new beverage?",
								"Confirmation Message", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,
								null, null);

						if (jawab == JOptionPane.YES_OPTION) {
							beverageController.addBeverage(newbeverageId, newbeverageName, newbeverageType,
									newbeveragePrice, String.valueOf(newbeverageStock));
							newBevId = beverageController.generateBeverageId();

							BeverageTableModel beverageTableModel = new BeverageTableModel(
									beverageController.getBeverageList());
							manageBeverageTable.setModel(beverageTableModel);

							newBeverageIdField.setText(newBevId);
							newBeverageNameField.setText("");
							newBeveragePriceField.setText("");
							newBeverageTypeComboBox.setSelectedIndex(0);
							newBeverageStockField.setValue(0);
						}

					} else {
						JOptionPane.showMessageDialog(null, "Invalid beverage data!");
					}
				} else {
					JOptionPane.showMessageDialog(null, "Please fill the beverage data!");
				}
			}
		});
		insertBeverageButton.setBounds(26, 420, 357, 23);
		frmManageBeverages.getContentPane().add(insertBeverageButton);

		JButton resetButton = new JButton("Reset");
		resetButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				beverageIdField.setText("");
				beverageNameField.setText("");
				beveragePriceField.setText("");
				beverageStockField.setText("");
				beverageTypeComboBox.setSelectedIndex(0);
				addStockField.setValue(0);

				newBeverageIdField.setText(newBevId);
				newBeverageNameField.setText("");
				newBeveragePriceField.setText("");
				newBeverageTypeComboBox.setSelectedIndex(0);
				newBeverageStockField.setValue(0);
			}
		});
		resetButton.setBounds(10, 454, 357, 23);
		frmManageBeverages.getContentPane().add(resetButton);
		
		JMenuBar menuBar = new JMenuBar();
		frmManageBeverages.setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Profile");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Main Form");
		mnNewMenu.add(mntmNewMenuItem);
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainForm2 mainForm = new MainForm2();
				mainForm.setVisibliMain(mainForm.window);
				frmManageBeverages.dispose();
			}
		});
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Log Off");
		mnNewMenu.add(mntmNewMenuItem_1);
		mntmNewMenuItem_1.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOptionDialogActionPerformed2(evt);
                frmManageBeverages.dispose();
            }
        });
		
		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Exit");
		mnNewMenu.add(mntmNewMenuItem_2);
		mntmNewMenuItem_2.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOptionDialogActionPerformed(evt);
            }
        });
		
		
		
		JMenu mnNewMenu_1 = new JMenu("Manage");
		menuBar.add(mnNewMenu_1);
		
		JMenuItem mntmNewMenuItem_3 = new JMenuItem("Manage Beverage");
		mnNewMenu_1.add(mntmNewMenuItem_3);
		mntmNewMenuItem_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ManageBeverageForm manage = new ManageBeverageForm();
				manage.setVisibliManage(manage.window);
				frmManageBeverages.dispose();
//				frmMainForm.setVisible(false);
			}
		});

	}

	private boolean addStockValidate(int stock) {
		if (stock > 0) {
			return true;
		}
		return false;

	}

	private boolean beverageUpdateValidate(String name, String type, String price) {
		boolean bname = false;
		boolean btype = false;
		boolean bprice = false;
		int iprice;

		try {
			iprice = Integer.parseInt(price);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		if (name.length() >= 5 && name.length() <= 30) {
			bname = true;
		}
		if (type.equals("Boba") || type.equals("Coffee") || type.equals("Tea") || type.equals("Smoothies")) {
			btype = true;
		}
		if (iprice > 0) {
			bprice = true;
		}
		return bname && btype && bprice;
	}

	private static class BeverageTableModel extends AbstractTableModel {
		private final String[] COLUMNS = { "Beverage Id", "Beverage Name", "Beverage Type", "Beverage Price",
				"Beverage Stock" };
		private List<Beverage> beverageList;

		private BeverageTableModel(List<Beverage> beverageList) {
			this.beverageList = beverageList;
		}

		@Override
		public int getRowCount() {
			return beverageList.size();
		}

		@Override
		public int getColumnCount() {
			return COLUMNS.length;
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			switch (columnIndex) {
			case 0:
				return beverageList.get(rowIndex).getId();
			case 1:
				return beverageList.get(rowIndex).getName();
			case 2:
				return beverageList.get(rowIndex).getType();
			case 3:
				return beverageList.get(rowIndex).getPrice();
			case 4:
				return beverageList.get(rowIndex).getStock();
			default:
				return '-';
			}
		}

		@Override
		public String getColumnName(int column) {
			return COLUMNS[column];
		}

		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (getValueAt(0, columnIndex) != null) {
				return getValueAt(0, columnIndex).getClass();
			} else {
				return Object.class;
			}
		}
	}
}
