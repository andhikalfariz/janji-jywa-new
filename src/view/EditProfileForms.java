package view;



import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import model.Users;
import java.util.*;
import java.util.regex.Pattern;

import controller.UsersController;

public class EditProfileForms {

	private JFrame frmEditProfile;
	private ButtonGroup G1;
	static EditProfileForms window;
	static UsersController userController = UsersController.getInstance();
	private JTextField username;
	private JTextField userEmail;
	private JTextField userPhone;
	private JTextField userAddress;
	private JPasswordField oldPassword;
	private JPasswordField newPassword;
	private JPasswordField confirmPassword;
	private String actionCommand = "";

	/**
	 * Launch the application.
	 */
	
	public static EditProfileForms login(){
		return window;
	};
	
	public void setVisibliProfile(EditProfileForms windows) {
		windows = new EditProfileForms();
		windows.frmEditProfile.setVisible(true);
	}
	
	
	static int MAX = 10;

	static boolean isDigit(char ch) {
		if (ch >= '0' && ch <= '9')
			return true;
		return false;
	}

	// Function that returns true
	// if str contains all the
	// digits from 0 to 9
	static boolean allDigits(String str, int len) {
		for (int i = 0; i < len; i++) {
			if (isDigit(str.charAt(i)) == false) {
				return false;
			}
		}
		return true;
	}
	
	
	public static boolean emailIsValid(String email) {
		String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." + "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z"
				+ "A-Z]{2,7}$";

		Pattern pat = Pattern.compile(emailRegex);
		if (email == null)
			return false;
		return pat.matcher(email).matches();
	}
	
	
	public static boolean isValidAddress(String address) {
		String[] listWords = address.split(" ");
		String lastWord = listWords[listWords.length - 1];
		if (lastWord.equalsIgnoreCase("street")) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isAlphanumeric(String str) {
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (!Character.isLetterOrDigit(c))
				return false;
		}
		return true;
	}
	
	
	private static void btnOptionDialogActionPerformed2(java.awt.event.ActionEvent evt) {

        int jawab = JOptionPane.showOptionDialog(null,
                "You sure want to exit?",
                "Warning",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE, null, null, null);

        if (jawab == JOptionPane.YES_OPTION) {
            JOptionPane.showMessageDialog(null, "Program exit");
            System.exit(0);
        }
	};
	
	private static void btnOptionDialogActionPerformed3(java.awt.event.ActionEvent evt) {
		JOptionPane.showMessageDialog(null, "You'll Log Off");
		userController.logout();
		LoginForm login = new LoginForm();
		login.setVisiblilogin(login.window);
			
//        int jawab = JOptionPane.showOptionDialog(null,
//                "You sure want to Log Off?",
//                "Warning",
//                JOptionPane.YES_NO_OPTION,
//                JOptionPane.QUESTION_MESSAGE, null, null, null);
//
//        if (jawab == JOptionPane.YES_OPTION) {
//            JOptionPane.showMessageDialog(null, "You'll Log Off");
//            userController.logout();
//            LoginForm login = new LoginForm();
//            login.setVisiblilogin(login.window);
////            frame.dispose();
// 
//        }
	};
	
	
	private static void btnOptionDialogActionPerformed(java.awt.event.ActionEvent evt, UsersController userControllers ,Users user , JTextField username,
			JTextField userEmail, JTextField userPhone, JTextField userAddress,String gender ) {
		
		String Username = username.getText();
		String UserEmail = userEmail.getText();
		String UserPhone = userPhone.getText();
		String UserAddress = userAddress.getText();
		String Gender = gender;

		if ((username.getText().equals("")) || (userEmail.getText().equals("")) || (userPhone.getText().equals(""))
				|| (userAddress.getText().equals(""))) {

			JOptionPane.showMessageDialog(null, "Fill all requirements!");
		}else if(Username.length() < 5 || Username.length() > 30) {
			JOptionPane.showMessageDialog(null, "Username must be between 5 - 30 characters!");
			
		}else if(emailIsValid(UserEmail) == false) {
			JOptionPane.showMessageDialog(null, "Email must be in valid format!"); 
		}else if(UserPhone.length() < 12
				|| allDigits(UserPhone, UserPhone.length()) == false) {
			JOptionPane.showMessageDialog(null,
					"Phone number must be numeric and more than equals 12 digits"); 
		}else if(isValidAddress(UserAddress) == false || UserAddress.length() < 10) {
			JOptionPane.showMessageDialog(null,
					"Address must consist of 10 or more characters and ends with � Street�");
		}
			 
			
		
		
		
		
		else {
			int jawab = JOptionPane.showOptionDialog(null, "You sure want to update profile?", "Warning",
					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);

			if (jawab == JOptionPane.YES_OPTION) {
				user.setName(Username);
				user.setEmail(UserEmail);
				user.setPhone(UserPhone);
				user.setAdress(UserAddress);
				user.setGender(Gender);
				
				try {
					userControllers.update(user,Username,UserEmail,UserPhone,UserAddress,Gender);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
//				System.out.println(Gender);
				JOptionPane.showMessageDialog(null, "Profile Update!");
//	            System.exit(0);
			}

		}
		

	};
	
	
	
private static void btnOptionDialogActionPerformed1(java.awt.event.ActionEvent evt, UsersController userControllers ,Users user ,JPasswordField oldPassword, JPasswordField newPassword, JPasswordField confirmPassword) {
		
		String newPass = new String(newPassword.getPassword());
		String confirmPass = new String(confirmPassword.getPassword());
		String currentPassword = new String(user.getPassword());
		String oldPass = new String(oldPassword.getPassword());
		
		if((oldPassword.getPassword().length == 0) || (newPassword.getPassword().length == 0) || (confirmPassword.getPassword().length == 0)) {
			
			JOptionPane.showMessageDialog(null, "Fill all requirements!");
			
		}else if (!newPass.equals(confirmPass)) {
			
			JOptionPane.showMessageDialog(null, "Password doesn't match");
			
			
		}else if(!oldPass.equals(currentPassword)) {
			System.out.println("current = " + currentPassword);
			System.out.println("old = " + oldPass);
			JOptionPane.showMessageDialog(null, "Your old password is incorrect");
		}else if((isAlphanumeric(newPass) == false) || newPass.length() < 5
				|| newPass.length() > 30) {
			JOptionPane.showMessageDialog(null,
					"Password must at least contain 1 character Password must 5 - 30 length of character and digit and 1 digit!"); {
			
					}
		}
		else {
			int jawab = JOptionPane.showOptionDialog(null,
	                "You sure want to change password?",
	                "Warning",
	                JOptionPane.YES_NO_OPTION,
	                JOptionPane.QUESTION_MESSAGE, null, null, null);

	        if (jawab == JOptionPane.YES_OPTION) {
	        	user.setPassword(confirmPass);
	        	System.out.println("current = " + currentPassword);
				System.out.println("old = " + oldPass);
	        	
				try {
					userControllers.updatePass(user, confirmPass);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        	
	            JOptionPane.showMessageDialog(null, "Password change!");
//	            System.exit(0);
	        }
			
		}
	};
	
	
	
	
	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EditProfileForms window = new EditProfileForms();
					window.frmEditProfile.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public EditProfileForms() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmEditProfile = new JFrame();
		frmEditProfile.setTitle("Edit Profile");
		frmEditProfile.getContentPane().setBackground(Color.CYAN);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		frmEditProfile.getContentPane().setLayout(gridBagLayout);
		
//		frmEditProfile.pack();
		
		Users userLog;
		
		
		
		UsersController userController = UsersController.getInstance();
		
		userLog = userController.getLoggedUser();
		
		
		JLabel lblNewLabel = new JLabel("Update Profile");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 0;
		frmEditProfile.getContentPane().add(lblNewLabel, gbc_lblNewLabel);
		
		JLabel lblNewLabel_9 = new JLabel("Change Password");
		GridBagConstraints gbc_lblNewLabel_9 = new GridBagConstraints();
		gbc_lblNewLabel_9.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_9.gridx = 5;
		gbc_lblNewLabel_9.gridy = 0;
		frmEditProfile.getContentPane().add(lblNewLabel_9, gbc_lblNewLabel_9);
		
		JLabel lblNewLabel_1 = new JLabel("Username");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 1;
		gbc_lblNewLabel_1.gridy = 1;
		frmEditProfile.getContentPane().add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		username = new JTextField();
		GridBagConstraints gbc_username = new GridBagConstraints();
		gbc_username.gridwidth = 2;
		gbc_username.insets = new Insets(0, 0, 5, 5);
		gbc_username.fill = GridBagConstraints.HORIZONTAL;
		gbc_username.gridx = 2;
		gbc_username.gridy = 1;
		frmEditProfile.getContentPane().add(username, gbc_username);
		username.setColumns(10);
		
		JLabel lblNewLabel_6 = new JLabel("Old Password");
		GridBagConstraints gbc_lblNewLabel_6 = new GridBagConstraints();
		gbc_lblNewLabel_6.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_6.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_6.gridx = 4;
		gbc_lblNewLabel_6.gridy = 1;
		frmEditProfile.getContentPane().add(lblNewLabel_6, gbc_lblNewLabel_6);
		
		oldPassword = new JPasswordField();
		GridBagConstraints gbc_oldPassword = new GridBagConstraints();
		gbc_oldPassword.insets = new Insets(0, 0, 5, 0);
		gbc_oldPassword.fill = GridBagConstraints.HORIZONTAL;
		gbc_oldPassword.gridx = 5;
		gbc_oldPassword.gridy = 1;
		frmEditProfile.getContentPane().add(oldPassword, gbc_oldPassword);
		
		JLabel lblNewLabel_2 = new JLabel("User Email");
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 1;
		gbc_lblNewLabel_2.gridy = 2;
		frmEditProfile.getContentPane().add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		userEmail = new JTextField();
		GridBagConstraints gbc_userEmail = new GridBagConstraints();
		gbc_userEmail.gridwidth = 2;
		gbc_userEmail.insets = new Insets(0, 0, 5, 5);
		gbc_userEmail.fill = GridBagConstraints.HORIZONTAL;
		gbc_userEmail.gridx = 2;
		gbc_userEmail.gridy = 2;
		frmEditProfile.getContentPane().add(userEmail, gbc_userEmail);
		userEmail.setColumns(10);
		
		JLabel lblNewLabel_7 = new JLabel("New Password");
		GridBagConstraints gbc_lblNewLabel_7 = new GridBagConstraints();
		gbc_lblNewLabel_7.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_7.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_7.gridx = 4;
		gbc_lblNewLabel_7.gridy = 2;
		frmEditProfile.getContentPane().add(lblNewLabel_7, gbc_lblNewLabel_7);
		
		newPassword = new JPasswordField();
		GridBagConstraints gbc_newPassword = new GridBagConstraints();
		gbc_newPassword.insets = new Insets(0, 0, 5, 0);
		gbc_newPassword.fill = GridBagConstraints.HORIZONTAL;
		gbc_newPassword.gridx = 5;
		gbc_newPassword.gridy = 2;
		frmEditProfile.getContentPane().add(newPassword, gbc_newPassword);
		
		JLabel lblNewLabel_3 = new JLabel("User Phone");
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_3.gridx = 1;
		gbc_lblNewLabel_3.gridy = 3;
		frmEditProfile.getContentPane().add(lblNewLabel_3, gbc_lblNewLabel_3);
		
		userPhone = new JTextField();
		GridBagConstraints gbc_userPhone = new GridBagConstraints();
		gbc_userPhone.gridwidth = 2;
		gbc_userPhone.insets = new Insets(0, 0, 5, 5);
		gbc_userPhone.fill = GridBagConstraints.HORIZONTAL;
		gbc_userPhone.gridx = 2;
		gbc_userPhone.gridy = 3;
		frmEditProfile.getContentPane().add(userPhone, gbc_userPhone);
		userPhone.setColumns(10);
		
		JLabel lblNewLabel_8 = new JLabel("Confirm Password");
		GridBagConstraints gbc_lblNewLabel_8 = new GridBagConstraints();
		gbc_lblNewLabel_8.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_8.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_8.gridx = 4;
		gbc_lblNewLabel_8.gridy = 3;
		frmEditProfile.getContentPane().add(lblNewLabel_8, gbc_lblNewLabel_8);
		
		confirmPassword = new JPasswordField();
		GridBagConstraints gbc_confirmPassword = new GridBagConstraints();
		gbc_confirmPassword.insets = new Insets(0, 0, 5, 0);
		gbc_confirmPassword.fill = GridBagConstraints.HORIZONTAL;
		gbc_confirmPassword.gridx = 5;
		gbc_confirmPassword.gridy = 3;
		frmEditProfile.getContentPane().add(confirmPassword, gbc_confirmPassword);
		
		JLabel lblNewLabel_4 = new JLabel("User Address");
		GridBagConstraints gbc_lblNewLabel_4 = new GridBagConstraints();
		gbc_lblNewLabel_4.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_4.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_4.gridx = 1;
		gbc_lblNewLabel_4.gridy = 4;
		frmEditProfile.getContentPane().add(lblNewLabel_4, gbc_lblNewLabel_4);
		
		userAddress = new JTextField();
		GridBagConstraints gbc_userAddress = new GridBagConstraints();
		gbc_userAddress.gridwidth = 2;
		gbc_userAddress.insets = new Insets(0, 0, 5, 5);
		gbc_userAddress.fill = GridBagConstraints.HORIZONTAL;
		gbc_userAddress.gridx = 2;
		gbc_userAddress.gridy = 4;
		frmEditProfile.getContentPane().add(userAddress, gbc_userAddress);
		userAddress.setColumns(10);
		
		JLabel lblNewLabel_5 = new JLabel("User Gender");
		GridBagConstraints gbc_lblNewLabel_5 = new GridBagConstraints();
		gbc_lblNewLabel_5.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_5.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_5.gridx = 1;
		gbc_lblNewLabel_5.gridy = 5;
		frmEditProfile.getContentPane().add(lblNewLabel_5, gbc_lblNewLabel_5);
		
		JRadioButton rdbtnMale = new JRadioButton("Male");
		rdbtnMale.setBackground(Color.CYAN);
		GridBagConstraints gbc_rdbtnMale = new GridBagConstraints();
		gbc_rdbtnMale.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnMale.gridx = 2;
		gbc_rdbtnMale.gridy = 5;
		frmEditProfile.getContentPane().add(rdbtnMale, gbc_rdbtnMale);
		
		JRadioButton rdbtnFemale = new JRadioButton("Female");
		rdbtnFemale.setBackground(Color.CYAN);
		GridBagConstraints gbc_rdbtnFemale = new GridBagConstraints();
		gbc_rdbtnFemale.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnFemale.gridx = 3;
		gbc_rdbtnFemale.gridy = 5;
		frmEditProfile.getContentPane().add(rdbtnFemale, gbc_rdbtnFemale);
		
		
		G1 = new ButtonGroup();
//		
		
		G1.add(rdbtnMale);
		G1.add(rdbtnFemale);
		
		
		
//		
		
		username.setText(userLog.getName());
		userEmail.setText(userLog.getEmail());
		userPhone.setText(userLog.getPhone());
		userAddress.setText(userLog.getAdress());
		
		if(userLog.isGender().equals("Male")) {
			rdbtnMale.setSelected(true);
		}else {
			rdbtnFemale.setSelected(true);
		}
		
		
		rdbtnMale.setActionCommand("Male");
		rdbtnFemale.setActionCommand("Female");
		
		
		
		
		
		JButton btnNewButton = new JButton("Update Profile");
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.gridwidth = 2;
		gbc_btnNewButton.insets = new Insets(0, 0, 0, 5);
		gbc_btnNewButton.gridx = 1;
		gbc_btnNewButton.gridy = 7;
		frmEditProfile.getContentPane().add(btnNewButton, gbc_btnNewButton);
		
		
		btnNewButton.addActionListener(new java.awt.event.ActionListener() {
			
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnOptionDialogActionPerformed(evt, userController ,userLog ,username, userEmail, userPhone, userAddress, G1.getSelection().getActionCommand());
			}
		});
		
		
//		String currentPass = new String(userLog.getPassword());
		
		
		JButton btnNewButton_1 = new JButton("Change Password");
		GridBagConstraints gbc_btnNewButton_1 = new GridBagConstraints();
		gbc_btnNewButton_1.gridwidth = 2;
		gbc_btnNewButton_1.insets = new Insets(0, 0, 0, 5);
		gbc_btnNewButton_1.gridx = 4;
		gbc_btnNewButton_1.gridy = 7;
		frmEditProfile.getContentPane().add(btnNewButton_1, gbc_btnNewButton_1);
		
		btnNewButton_1.addActionListener(new java.awt.event.ActionListener() {
			
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnOptionDialogActionPerformed1(evt,userController ,userLog ,oldPassword, newPassword, confirmPassword);
			}
		});
		
		
//		frmEditProfile.setBackground(Color.CYAN);
		
		
		JMenuBar menuBar = new JMenuBar();
		frmEditProfile.setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Profile");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Main Form");
		mnNewMenu.add(mntmNewMenuItem);
		
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainForm2 mainForm = new MainForm2();
				mainForm.setVisibliMain(mainForm.window);
				frmEditProfile.dispose();
			}
		});
		
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Log Off");
		mnNewMenu.add(mntmNewMenuItem_1);
		
		mntmNewMenuItem_1.addActionListener(new java.awt.event.ActionListener() {
			
			 public void actionPerformed(java.awt.event.ActionEvent evt) {
			        btnOptionDialogActionPerformed3(evt);
			         frmEditProfile.dispose();
			}
		});
		
		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Exit");
		mnNewMenu.add(mntmNewMenuItem_2);
		
		
		mntmNewMenuItem_2.addActionListener(new java.awt.event.ActionListener() {
			
			  public void actionPerformed(java.awt.event.ActionEvent evt) {
			                btnOptionDialogActionPerformed2(evt);
			 }
		});
		
		
		
		
//		username.setText(userLog.getName());
//		userEmail.setText(userLog.getEmail());
//		userPhone.setText(userLog.getPhone());
//		userAddress.setText(userLog.getAdress());
//		
//		if(userLog.isGender().equals("Male")) {
//			rdbtnMale.setSelected(true);
//		}else {
//			rdbtnFemale.setSelected(true);
//		}
//		
//		
//		rdbtnMale.setActionCommand("Male");
//		rdbtnFemale.setActionCommand("Female");
		
//		System.out.println(G1.getSelection().getActionCommand()); 
		
		
		
		if(userLog.isRole().equals("Admin")) {
			JMenu mnNewMenu_1 = new JMenu("Manage");
			menuBar.add(mnNewMenu_1);
			
			JMenuItem mntmNewMenuItem_3 = new JMenuItem("Manage Beverages");
			mnNewMenu_1.add(mntmNewMenuItem_3);
//			frmEditProfile.getContentPane().setLayout(new BorderLayout(0, 0));
			
			
			
		}else {
			JMenu mnNewMenu_1 = new JMenu("Transaction");
			menuBar.add(mnNewMenu_1);
			
			JMenuItem mntmNewMenuItem_3 = new JMenuItem("Buy Beverages");
			mnNewMenu_1.add(mntmNewMenuItem_3);
		
			JMenuItem mntmNewMenuItem_4 = new JMenuItem("View Transaction History");
			mnNewMenu_1.add(mntmNewMenuItem_4);			
		}
		
		
		
		
		frmEditProfile.setBounds(100, 100, 450, 300);
		frmEditProfile.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		
//		frmEditProfile.getContentPane().setBackground(Color.CYAN);
//		frmEditProfile.setTitle("Edit Profile");
//		frmEditProfile.setBounds(100, 100, 450, 300);
//		frmEditProfile.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
//		JMenuBar menuBar = new JMenuBar();
//		frmEditProfile.setJMenuBar(menuBar);
//		
//		JMenu mnNewMenu = new JMenu("Profile");
//		menuBar.add(mnNewMenu);
//		
//		JMenuItem mntmNewMenuItem = new JMenuItem("Main Form");
//		mnNewMenu.add(mntmNewMenuItem);
//		
//		mntmNewMenuItem.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				MainForm2 mainForm = new MainForm2();
//				mainForm.setVisibliMain(mainForm.window);
//				frmEditProfile.dispose();
////				frmMainForm.setVisible(false);
//			}
//		});
//		
//		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Log Off");
//		mnNewMenu.add(mntmNewMenuItem_1);
//		
//		mntmNewMenuItem_1.addActionListener(new java.awt.event.ActionListener() {
//
//            public void actionPerformed(java.awt.event.ActionEvent evt) {
//                btnOptionDialogActionPerformed3(evt);
//                frmEditProfile.dispose();
//            }
//        });
//		
//		
//		
//		
//		
//		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Exit");
//		mnNewMenu.add(mntmNewMenuItem_2);
//		
//		
//		
//		mntmNewMenuItem_2.addActionListener(new java.awt.event.ActionListener() {
//
//            public void actionPerformed(java.awt.event.ActionEvent evt) {
//                btnOptionDialogActionPerformed2(evt);
//            }
//        });
		
		
		
		
//		G1 = new ButtonGroup();
//		G1.add(rdbtnNewRadioButton);
//		G1.add(rdbtnNewRadioButton_1);
		
		
//		JButton btnNewButton = new JButton("Update Profile");
//		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
//		gbc_btnNewButton.gridwidth = 2;
//		gbc_btnNewButton.insets = new Insets(0, 0, 0, 5);
//		gbc_btnNewButton.gridx = 2;
//		gbc_btnNewButton.gridy = 7;
//		frmEditProfile.getContentPane().add(btnNewButton, gbc_btnNewButton);
//		
//		btnNewButton.addActionListener(new java.awt.event.ActionListener() {
//
//			public void actionPerformed(java.awt.event.ActionEvent evt) {
//				btnOptionDialogActionPerformed(evt, username, userEmail, userPhone, userAddress);
//			}
//		});
//		
//		
//		JButton btnNewButton_1 = new JButton("Change password");
//		GridBagConstraints gbc_btnNewButton_1 = new GridBagConstraints();
//		gbc_btnNewButton_1.gridwidth = 2;
//		gbc_btnNewButton_1.insets = new Insets(0, 0, 0, 5);
//		gbc_btnNewButton_1.gridx = 5;
//		gbc_btnNewButton_1.gridy = 7;
//		frmEditProfile.getContentPane().add(btnNewButton_1, gbc_btnNewButton_1);
//		
//		
//		btnNewButton_1.addActionListener(new java.awt.event.ActionListener() {
//
//
//
//			public void actionPerformed(java.awt.event.ActionEvent evt) {
//				btnOptionDialogActionPerformed1(evt, oldPassword, newPassword, confirmPassword);
//			}
//		});
//		
//		
//		
////		String generateIds;
//		Users userLog;
//		
////		generateIds = UsersController.generateId();
//		
//		UsersController userController = UsersController.getInstance();
//		
//		userLog = userController.getLoggedUser();
//		
//		username.setText(userLog.getName());
//		userEmail.setText(userLog.getEmail());
//		userPhone.setText(userLog.getPhone());
//		userAddress.setText(userLog.getAdress());
//		
//		if(userLog.isGender().equals("Male")) {
//			rdbtnNewRadioButton_1.setSelected(true);
//		}else {
//			rdbtnNewRadioButton.setSelected(true);
//		}
//		
		
//		if(userLog.isRole().equals("Admin")) {
//			JMenu mnNewMenu_1 = new JMenu("Manage");
//			menuBar.add(mnNewMenu_1);
//			
//			JMenuItem mntmNewMenuItem_3 = new JMenuItem("Manage Beverages");
//			mnNewMenu_1.add(mntmNewMenuItem_3);
//			frmEditProfile.getContentPane().setLayout(new BorderLayout(0, 0));
//			
//			
//			
//		}else {
//			JMenu mnNewMenu_1 = new JMenu("Transaction");
//			menuBar.add(mnNewMenu_1);
//			
//			JMenuItem mntmNewMenuItem_3 = new JMenuItem("Buy Beverages");
//			mnNewMenu_1.add(mntmNewMenuItem_3);
//		
//			JMenuItem mntmNewMenuItem_4 = new JMenuItem("View Transaction History");
//			mnNewMenu_1.add(mntmNewMenuItem_4);			
//		}
		
		
	}

}
