package view;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import java.awt.BorderLayout;
import java.awt.Color;

import model.Beverage;
import model.DetailTransaction;
import model.HeaderTransaction;
import model.Users;
import controller.TransactionController;
import controller.UsersController;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

public class TransactionForm {

	private JFrame frmTransactionHistory;
	private JTable transactionTable;
	private JTextField selectedIdField;
	private JTable resultTable;
	private JTextField grandTotalField;
	private TransactionController transactionController;
	static TransactionForm window;
	private JMenuBar menuBar;
	static Users userlog;
	static UsersController userController = UsersController.getInstance();

	/**
	 * Launch the application.
	 */
	
	
	public static TransactionForm login(){
		return window;
	};
	
	public void setVisibliTransaction(TransactionForm windows) {
		windows = new TransactionForm();
		windows.frmTransactionHistory.setVisible(true);
	}
	
	
	
	
	private static void btnOptionDialogActionPerformed(java.awt.event.ActionEvent evt) {

        int jawab = JOptionPane.showOptionDialog(null,
                "You sure want to exit?",
                "Warning",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE, null, null, null);

        if (jawab == JOptionPane.YES_OPTION) {
            JOptionPane.showMessageDialog(null, "Program exit");
            System.exit(0);
        }
	};
	
	private static void btnOptionDialogActionPerformed2(java.awt.event.ActionEvent evt) {
		JOptionPane.showMessageDialog(null, "You'll Log Off");
		userController.logout();
		LoginForm login = new LoginForm();
		login.setVisiblilogin(login.window);

//        int jawab = JOptionPane.showOptionDialog(null,
//                "You sure want to Log Off?",
//                "Warning",
//                JOptionPane.YES_NO_OPTION,
//                JOptionPane.QUESTION_MESSAGE, null, null, null);
//
//        if (jawab == JOptionPane.YES_OPTION) {
//            JOptionPane.showMessageDialog(null, "You'll Log Off");
//            userController.logout();
//            LoginForm login = new LoginForm();
//            login.setVisiblilogin(login.window);
////            frame.dispose();
// 
//        }
	};
	
	
	
	
	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					window = new TransactionForm();
					window.frmTransactionHistory.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TransactionForm() {
		transactionController = TransactionController.getInstance();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmTransactionHistory = new JFrame();
		frmTransactionHistory.setTitle("Transaction History");
		frmTransactionHistory.getContentPane().setBackground(Color.CYAN);
		frmTransactionHistory.getContentPane().setForeground(Color.WHITE);
		frmTransactionHistory.setBounds(100, 100, 823, 736);
		frmTransactionHistory.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTransactionHistory.getContentPane().setLayout(null);

		JLabel transactionHistoryLabel = new JLabel("Transaction History");
		transactionHistoryLabel.setHorizontalAlignment(SwingConstants.CENTER);
		transactionHistoryLabel.setBounds(10, 11, 789, 14);
		frmTransactionHistory.getContentPane().add(transactionHistoryLabel);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 49, 789, 254);
		frmTransactionHistory.getContentPane().add(scrollPane);

		transactionTable = new JTable();
		transactionTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int i = transactionTable.getSelectedRow();
				String trId = transactionTable.getValueAt(i, 0).toString();
				selectedIdField.setText(trId);
				
				ResultTableModel resultTableModel = new ResultTableModel(transactionController.getDetailTransactionListById(trId) );
				resultTable.setModel(resultTableModel);
				
				Integer total = transactionController.getTotalDetailTransactionPrice(trId);	
				grandTotalField.setText(total.toString() );
			}
		});
		
		
		scrollPane.setViewportView(transactionTable);
		TransactionTableModel transactionTableModel = new TransactionTableModel(transactionController.getTransactionList());
		transactionTable.setModel(transactionTableModel);

		JLabel selectedIdLabel = new JLabel("Selected ID");
		selectedIdLabel.setBounds(10, 324, 71, 14);
		frmTransactionHistory.getContentPane().add(selectedIdLabel);

		selectedIdField = new JTextField();
		selectedIdField.setBounds(91, 321, 150, 20);
		frmTransactionHistory.getContentPane().add(selectedIdField);
		selectedIdField.setColumns(10);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 362, 789, 220);
		frmTransactionHistory.getContentPane().add(scrollPane_1);

		resultTable = new JTable();
		scrollPane_1.setViewportView(resultTable);
		resultTable.setModel(new DefaultTableModel(new Object[][] { { null, null, null, null, null, null, "" }, },
				new String[] { "Transaction ID", "Beverage ID", "Beverage Name", "Beverage Type", "Beverage Price",
						"Beverage Quantity", "Sub Total" }));

		JLabel grandTotalLabel = new JLabel("Grand Total");
		grandTotalLabel.setBounds(509, 602, 101, 14);
		frmTransactionHistory.getContentPane().add(grandTotalLabel);

		grandTotalField = new JTextField();
		grandTotalField.setBounds(620, 599, 167, 20);
		frmTransactionHistory.getContentPane().add(grandTotalField);
		grandTotalField.setColumns(10);
		
		
		
		
		
		
		menuBar = new JMenuBar();
		frmTransactionHistory.setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Profile");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Main Form");
		mnNewMenu.add(mntmNewMenuItem);
		
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainForm2 mainForm = new MainForm2();
				mainForm.setVisibliMain(mainForm.window);
				frmTransactionHistory.dispose();
			}
		});
		
		
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Log Off");
		mnNewMenu.add(mntmNewMenuItem_1);
		
		mntmNewMenuItem_1.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOptionDialogActionPerformed2(evt);
                frmTransactionHistory.dispose();
            }
        });
		
		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Exit");
		mnNewMenu.add(mntmNewMenuItem_2);
		
		
		mntmNewMenuItem_2.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOptionDialogActionPerformed(evt);
            }
        });
		
		
		
//		userlog = userController.getLoggedUser();
//		
//		
//		
//		if(userlog.isRole().equals("Admin")) {
//			JMenu mnNewMenu_1 = new JMenu("Manage");
//			menuBar.add(mnNewMenu_1);
//			
//			JMenuItem mntmNewMenuItem_3 = new JMenuItem("Manage Beverages");
//			mnNewMenu_1.add(mntmNewMenuItem_3);
////			frmMainForm.getContentPane().setLayout(new BorderLayout(0, 0));
//			
//			
//			
//		}else {
//			JMenu mnNewMenu_1 = new JMenu("Transaction");
//			menuBar.add(mnNewMenu_1);
//			
//			JMenuItem mntmNewMenuItem_3 = new JMenuItem("Buy Beverages");
//			mnNewMenu_1.add(mntmNewMenuItem_3);
//		
//			JMenuItem mntmNewMenuItem_4 = new JMenuItem("View Transaction History");
//			mnNewMenu_1.add(mntmNewMenuItem_4);	
//			
//			mntmNewMenuItem_4.addActionListener(new ActionListener() {
//				public void actionPerformed(ActionEvent e) {
//					TransactionForm transaction = new TransactionForm();
//					transaction.setVisibliTransaction(transaction.window);
//					frmTransactionHistory.dispose();
////					frmMainForm.setVisible(false);
//				}
//			});
//		}
		
		
		
		
		JMenu mnNewMenu_1 = new JMenu("Transaction");
		menuBar.add(mnNewMenu_1);
		
		JMenuItem mntmNewMenuItem_3 = new JMenuItem("Buy Beverage");
		mnNewMenu_1.add(mntmNewMenuItem_3);
		mntmNewMenuItem_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BuyBeverageForm buy = new BuyBeverageForm();
				buy.setVisibliBuy(buy.window);
				frmTransactionHistory.dispose();
//				frmMainForm.setVisible(false);
			}
		});
		
		JMenuItem mntmNewMenuItem_4 = new JMenuItem("View Transaction History");
		mnNewMenu_1.add(mntmNewMenuItem_4);
		
		
		mntmNewMenuItem_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TransactionForm transaction = new TransactionForm();
				transaction.setVisibliTransaction(transaction.window);
				frmTransactionHistory.dispose();
//				frmMainForm.setVisible(false);
			}
		});
		
		
		resultTable.getColumnModel().getColumn(0).setPreferredWidth(87);
		resultTable.getColumnModel().getColumn(2).setPreferredWidth(83);
		transactionTable.getColumnModel().getColumn(0).setPreferredWidth(102);
		transactionTable.getColumnModel().getColumn(1).setPreferredWidth(103);
		transactionTable.getColumnModel().getColumn(2).setPreferredWidth(110);
	}
	
	private static class TransactionTableModel extends AbstractTableModel{
        private final String[] COLUMNS = {"Transaction ID","User ID", "Transaction Date",};
        private List<HeaderTransaction> transactionList;

        private TransactionTableModel(List<HeaderTransaction> transactionList){
            this.transactionList = transactionList;
        }

        @Override
        public int getRowCount() {
            return transactionList.size();
        }

        @Override
        public int getColumnCount() {
            return COLUMNS.length;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
        	switch (columnIndex){
                case 0:
                	return transactionList.get(rowIndex).getId();
                case 1:
                	return transactionList.get(rowIndex).getUserId();
                case 2:
                	return transactionList.get(rowIndex).getDate();
                default:
                	return '-';
            }
        }

        @Override
        public String getColumnName(int column) {
            return COLUMNS[column];
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            if (getValueAt(0,columnIndex) != null){
                return getValueAt(0,columnIndex).getClass();
            }
            else{
                return Object.class;
            }
        }
    }
	
	private static class ResultTableModel extends AbstractTableModel{
        private final String[] COLUMNS = {"Transaction ID","Beverage ID", "Beverage Name", "Beverage Type","Beverage Price", "Beverage quantity", "Subtotal"};
        private List<DetailTransaction> transactionList;

        private ResultTableModel(List<DetailTransaction> transactionList){
            this.transactionList = transactionList;
        }

        @Override
        public int getRowCount() {
            return transactionList.size();
        }

        @Override
        public int getColumnCount() {
            return COLUMNS.length;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
        	switch (columnIndex){
                case 0:
                	return transactionList.get(rowIndex).getHeaderTransactionId();
                case 1:
                	return transactionList.get(rowIndex).getBeverageId();
                case 2:
                	return transactionList.get(rowIndex).getBeverageName();
                case 3:
                	return transactionList.get(rowIndex).getBeveragetype();
                case 4:
                	return transactionList.get(rowIndex).getBeveragePrice();
                case 5:
                	return transactionList.get(rowIndex).getQuantity();
                case 6:
                	return transactionList.get(rowIndex).getSubtotal();
                default:
                	return '-';
            }
        }

        @Override
        public String getColumnName(int column) {
            return COLUMNS[column];
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            if (getValueAt(0,columnIndex) != null){
                return getValueAt(0,columnIndex).getClass();
            }
            else{
                return Object.class;
            }
        }
    }
}
