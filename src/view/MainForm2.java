package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import controller.UsersController;
import model.Users;
import java.awt.Color;

public class MainForm2 {
	static MainForm2 window;
	private JFrame frmMainForm;
	static Users userlog;
	String usernameLog;
	static UsersController userController = UsersController.getInstance();
	static boolean check = false;
	/**
	 * Launch the application.
	 */
	
	public static MainForm2 login(){
		return window;
	};
	
	public void setVisibliMain(MainForm2 windows) {
		windows = new MainForm2();
		windows.frmMainForm.setVisible(true);
	}
	
	
	private static void btnOptionDialogActionPerformed(java.awt.event.ActionEvent evt) {

        int jawab = JOptionPane.showOptionDialog(null,
                "You sure want to exit?",
                "Warning",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE, null, null, null);

        if (jawab == JOptionPane.YES_OPTION) {
            JOptionPane.showMessageDialog(null, "Program exit");
            System.exit(0);
        }
	};
	
	private static void btnOptionDialogActionPerformed2(java.awt.event.ActionEvent evt) {
		JOptionPane.showMessageDialog(null, "You'll Log Off");
		userController.logout();
		LoginForm login = new LoginForm();
		login.setVisiblilogin(login.window);
      
//        int jawab = JOptionPane.showOptionDialog(null,
//                "You sure want to Log Off?",
//                "Warning",
//                JOptionPane.YES_NO_OPTION,
//                JOptionPane.QUESTION_MESSAGE, null, null, null);
//
//        if (jawab == JOptionPane.YES_OPTION) {
//            JOptionPane.showMessageDialog(null, "You'll Log Off");
//            userController.logout();
//            LoginForm login = new LoginForm();
//            login.setVisiblilogin(login.window);
//            check = true;
////            window.frmMainForm.dispose();
////            window.frmMainForm.setVisible(false);
////            frame.dispose();
// 
//        }
	};
	
	
	
	
	
	
	
	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainForm2 window = new MainForm2();
					window.frmMainForm.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainForm2() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMainForm = new JFrame();
		frmMainForm.setBackground(Color.CYAN);
		frmMainForm.getContentPane().setBackground(Color.CYAN);
		frmMainForm.setTitle("Main Form");
		frmMainForm.setBounds(100, 100, 450, 300);
		frmMainForm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBackground(Color.WHITE);
		frmMainForm.setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Profile");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Edit Profile");
		mnNewMenu.add(mntmNewMenuItem);
		
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EditProfileForms profile = new EditProfileForms();
				profile.setVisibliProfile(profile.window);
				frmMainForm.dispose();
//				frmMainForm.setVisible(false);
			}
		});
		
		
		
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Log Off");
		mnNewMenu.add(mntmNewMenuItem_1);
		
		mntmNewMenuItem_1.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOptionDialogActionPerformed2(evt);
//                if(check = true) {
////                	frmMainForm.dispose();
//                	frmMainForm.setVisible(false);
//                }
                frmMainForm.dispose();
//                frmMainForm.dispose();
            }
        });
		
		
		
		
		
		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Exit");
		mnNewMenu.add(mntmNewMenuItem_2);
		
		
		
		mntmNewMenuItem_2.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOptionDialogActionPerformed(evt);
            }
        });
		
		
		
		
//		JMenu mnNewMenu_1 = new JMenu("Transaction");
//		menuBar.add(mnNewMenu_1);
//		
//		JMenuItem mntmNewMenuItem_3 = new JMenuItem("Buy Beverages");
//		mnNewMenu_1.add(mntmNewMenuItem_3);
//	
//		JMenuItem mntmNewMenuItem_4 = new JMenuItem("View Transaction History");
//		mnNewMenu_1.add(mntmNewMenuItem_4);			
		
		
		userlog = userController.getLoggedUser();
		usernameLog = userlog.getName();
		
		
		if(userlog.isRole().equals("Admin")) {
			JMenu mnNewMenu_1 = new JMenu("Manage");
			menuBar.add(mnNewMenu_1);
			
			JMenuItem mntmNewMenuItem_3 = new JMenuItem("Manage Beverages");
			mnNewMenu_1.add(mntmNewMenuItem_3);
			frmMainForm.getContentPane().setLayout(new BorderLayout(0, 0));
			mntmNewMenuItem_3.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					ManageBeverageForm manage = new ManageBeverageForm();
					manage.setVisibliManage(manage.window);
					frmMainForm.dispose();
//					frmMainForm.setVisible(false);
				}
			});
			
			
			
		}else {
			JMenu mnNewMenu_1 = new JMenu("Transaction");
			menuBar.add(mnNewMenu_1);
			
			JMenuItem mntmNewMenuItem_3 = new JMenuItem("Buy Beverages");
			mnNewMenu_1.add(mntmNewMenuItem_3);
			mntmNewMenuItem_3.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					BuyBeverageForm buy = new BuyBeverageForm();
					buy.setVisibliBuy(buy.window);
					frmMainForm.dispose();
//					frmMainForm.setVisible(false);
				}
			});
		
			JMenuItem mntmNewMenuItem_4 = new JMenuItem("View Transaction History");
			mnNewMenu_1.add(mntmNewMenuItem_4);	
			
			mntmNewMenuItem_4.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					TransactionForm transaction = new TransactionForm();
					transaction.setVisibliTransaction(transaction.window);
					frmMainForm.dispose();
//					frmMainForm.setVisible(false);
				}
			});
			
			
			
		}
		
		
		JLabel lblNewLabel = new JLabel("Welcome to Janji Jywa, " +usernameLog);
		lblNewLabel.setBackground(Color.CYAN);
//		JLabel lblNewLabel = new JLabel("Welcome to Janji Jywa, ");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		frmMainForm.getContentPane().add(lblNewLabel, BorderLayout.CENTER);
		
		
	}

}
