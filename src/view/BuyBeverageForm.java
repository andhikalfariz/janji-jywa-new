package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Color;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JSpinner;
import javax.swing.table.AbstractTableModel;

import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;

import controller.BeverageController;
import model.Beverage;
import controller.CartController;
import controller.UsersController;
import model.Cart;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.SpinnerNumberModel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class BuyBeverageForm {

	private JFrame frmBuyBeverages;
	private JTable table;
	private JTextField idField;
	private JTextField nameField;
	private JTextField typeField;
	private JTextField priceField;
	private JTextField stockField;
	private JTable table_1;
	private JScrollPane scrollPane_1;
	private JButton removeButton;
	private JButton checkoutButton;
	private JButton clearButton;
	CartController cartController;
	BeverageController beverageController;
	private JMenuBar menuBar;
	private JMenu mnNewMenu;
	private JMenu mnNewMenu_1;
	private JMenuItem mntmNewMenuItem;
	private JMenuItem mntmNewMenuItem_1;
	private JMenuItem mntmNewMenuItem_2;
	private JMenuItem mntmNewMenuItem_3;
	private JMenuItem mntmNewMenuItem_4;
	static UsersController userController = UsersController.getInstance();
	static BuyBeverageForm window;

	/**
	 * Launch the application.
	 */
	
	
	
	public static BuyBeverageForm login(){
		return window;
	};
	
	public void setVisibliBuy(BuyBeverageForm windows) {
		windows = new BuyBeverageForm();
		windows.frmBuyBeverages.setVisible(true);
	}
	
	
	
	
	private static void btnOptionDialogActionPerformed(java.awt.event.ActionEvent evt) {

        int jawab = JOptionPane.showOptionDialog(null,
                "You sure want to exit?",
                "Warning",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE, null, null, null);

        if (jawab == JOptionPane.YES_OPTION) {
            JOptionPane.showMessageDialog(null, "Program exit");
            System.exit(0);
        }
	};
	
	private static void btnOptionDialogActionPerformed2(java.awt.event.ActionEvent evt) {
		JOptionPane.showMessageDialog(null, "You'll Log Off");
		userController.logout();
		LoginForm login = new LoginForm();
		login.setVisiblilogin(login.window);

//        int jawab = JOptionPane.showOptionDialog(null,
//                "You sure want to Log Off?",
//                "Warning",
//                JOptionPane.YES_NO_OPTION,
//                JOptionPane.QUESTION_MESSAGE, null, null, null);
//
//        if (jawab == JOptionPane.YES_OPTION) {
//            JOptionPane.showMessageDialog(null, "You'll Log Off");
//            userController.logout();
//            LoginForm login = new LoginForm();
//            login.setVisiblilogin(login.window);
////            frame.dispose();
// 
//        }
	};
	
	
	
	
	
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					window = new BuyBeverageForm();
					window.frmBuyBeverages.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public BuyBeverageForm() {
		beverageController = BeverageController.getInstance();
		cartController = CartController.getInstance();

		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmBuyBeverages = new JFrame();
		frmBuyBeverages.setTitle("Buy Beverages");
		frmBuyBeverages.getContentPane().setBackground(Color.CYAN);
		frmBuyBeverages.setBounds(100, 100, 825, 636);
		frmBuyBeverages.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmBuyBeverages.getContentPane().setLayout(null);

		JLabel lblNewLabel = new JLabel("Buy Beverage");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(10, 11, 789, 14);
		frmBuyBeverages.getContentPane().add(lblNewLabel);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 36, 789, 205);
		frmBuyBeverages.getContentPane().add(scrollPane);

		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int i = table.getSelectedRow();
				idField.setText(table.getValueAt(i, 0).toString());
				nameField.setText(table.getValueAt(i, 1).toString());
				typeField.setText(table.getValueAt(i, 2).toString());
				priceField.setText(table.getValueAt(i, 3).toString());
				stockField.setText(table.getValueAt(i, 4).toString());
			}
		});
		scrollPane.setViewportView(table);
		BeverageTableModel beverageTableModel = new BeverageTableModel(beverageController.getBeverageList());

		table.setModel(beverageTableModel);

		table.getColumnModel().getColumn(0).setPreferredWidth(116);
		table.getColumnModel().getColumn(1).setPreferredWidth(117);
		table.getColumnModel().getColumn(2).setPreferredWidth(118);
		table.getColumnModel().getColumn(3).setPreferredWidth(111);
		table.getColumnModel().getColumn(4).setPreferredWidth(118);

		JLabel lblNewLabel_1 = new JLabel("Beverage ID");
		lblNewLabel_1.setBounds(10, 253, 86, 14);
		frmBuyBeverages.getContentPane().add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Beverage Name");
		lblNewLabel_2.setBounds(10, 278, 99, 14);
		frmBuyBeverages.getContentPane().add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("Beverage Type ");
		lblNewLabel_3.setBounds(10, 303, 134, 14);
		frmBuyBeverages.getContentPane().add(lblNewLabel_3);

		idField = new JTextField();
		idField.setEditable(false);
		idField.setBounds(190, 250, 153, 20);
		frmBuyBeverages.getContentPane().add(idField);
		idField.setColumns(10);

		nameField = new JTextField();
		nameField.setEditable(false);
		nameField.setBounds(190, 275, 153, 20);
		frmBuyBeverages.getContentPane().add(nameField);
		nameField.setColumns(10);

		typeField = new JTextField();
		typeField.setEditable(false);
		typeField.setBounds(190, 300, 153, 20);
		frmBuyBeverages.getContentPane().add(typeField);
		typeField.setColumns(10);

		JLabel lblNewLabel_4 = new JLabel("Beverage Price");
		lblNewLabel_4.setBounds(444, 253, 104, 14);
		frmBuyBeverages.getContentPane().add(lblNewLabel_4);

		JLabel lblNewLabel_5 = new JLabel("Beverage Stock");
		lblNewLabel_5.setBounds(444, 278, 125, 14);
		frmBuyBeverages.getContentPane().add(lblNewLabel_5);

		JLabel lblNewLabel_6 = new JLabel("Beverage Quantity");
		lblNewLabel_6.setBounds(444, 303, 104, 14);
		frmBuyBeverages.getContentPane().add(lblNewLabel_6);

		priceField = new JTextField();
		priceField.setEditable(false);
		priceField.setBounds(621, 250, 153, 20);
		frmBuyBeverages.getContentPane().add(priceField);
		priceField.setColumns(10);

		stockField = new JTextField();
		stockField.setEditable(false);
		stockField.setBounds(621, 275, 153, 20);
		frmBuyBeverages.getContentPane().add(stockField);
		stockField.setColumns(10);

		JSpinner quantityField = new JSpinner();
		quantityField.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		quantityField.setBounds(621, 300, 153, 20);
		frmBuyBeverages.getContentPane().add(quantityField);

		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 392, 787, 139);
		frmBuyBeverages.getContentPane().add(scrollPane_1);

		table_1 = new JTable();

		scrollPane_1.setViewportView(table_1);
		table_1.setModel(refreshCartTableModel());

		JButton addButton = new JButton("Add to Cart");
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int itemQuantity = (Integer) quantityField.getValue();
				int itemStock = 0;
				if (!stockField.getText().equals("")) {
					itemStock = Integer.parseInt(stockField.getText());
				}

				if (itemStock == 0) {
					JOptionPane.showMessageDialog(null, "Please select the beverage!");
				}

				else {
					if (itemQuantity > itemStock) {
						JOptionPane.showMessageDialog(null, "There is no more stock for this beverage!");
					} else if (itemQuantity < 1) {
						JOptionPane.showMessageDialog(null, "Please add quantity!");
					} else {

						int idx = cartController.checkBeverageInCart(idField.getText());
						if (idx != -1000) {
							int quantitynow = cartController.getCartQuantity(idx);
							int newQuantity = quantitynow += itemQuantity;
							if (newQuantity <= itemStock) {
								cartController.setCartQuantity(idx, newQuantity);

								table_1.setModel(refreshCartTableModel());
								JOptionPane.showMessageDialog(null, "Succesfully insert to cart!");
							} else {
								JOptionPane.showMessageDialog(null, "There is no more stock for this beverage!");
							}
						} else {
							String beverageid = idField.getText();
							String beveragename = nameField.getText();
							String beveragetype = typeField.getText();
							int beverageprice = Integer.parseInt(priceField.getText());
							int beveragestock = Integer.parseInt(stockField.getText());
							int subtotal = beverageprice * beveragestock;

							Cart newItem = new Cart("US001", beverageid, beveragename, beveragetype, beverageprice,
									beveragestock, itemQuantity, subtotal);
							cartController.addCartToList(newItem);

							table_1.setModel(refreshCartTableModel());
							JOptionPane.showMessageDialog(null, "Succesfully insert to cart!");

						}

					}
				}

			}
		});
		addButton.setBounds(342, 332, 125, 35);
		frmBuyBeverages.getContentPane().add(addButton);

		removeButton = new JButton("Remove Selected Cart");
		removeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (table_1.getSelectedRow() != -1) {
					int row = table_1.getSelectedRow();
					String idbeverage = table_1.getValueAt(row, 0).toString();
					int idx = cartController.checkBeverageInCart(idbeverage);

					cartController.removeCartFromList(idx);

					table_1.setModel(refreshCartTableModel());
				}
			}
		});
		removeButton.setBounds(10, 565, 270, 23);
		frmBuyBeverages.getContentPane().add(removeButton);

		checkoutButton = new JButton("Checkout");
		checkoutButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(cartController.getCartListSize() != 0 ) {
					int jawab = JOptionPane.showOptionDialog(null, 
							"Are you sure you want to checkout cart?",
							"Confirmation Message", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null,
							null);

					if (jawab == JOptionPane.YES_OPTION) {
						cartController.checkOutCart();

						table_1.setModel(refreshCartTableModel());
						
						BeverageTableModel beverageTableModel = new BeverageTableModel(beverageController.getBeverageList());
						table.setModel(beverageTableModel);
						JOptionPane.showMessageDialog(null, "Succesfully Checkout Cart!");
					}
				}
			}
		});
		checkoutButton.setBounds(540, 565, 259, 23);
		frmBuyBeverages.getContentPane().add(checkoutButton);

		clearButton = new JButton("Clear Cart");
		clearButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (cartController.getCartListSize() != 0) {
					int jawab = JOptionPane.showOptionDialog(null, "Are you sure you want to clear cart?",
							"Confirmation Message", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null,
							null);

					if (jawab == JOptionPane.YES_OPTION) {
						cartController.clearCartList();

						table_1.setModel(refreshCartTableModel());
						
					}
				}
			}

		});

		clearButton.setBounds(290, 565, 240, 23);
		frmBuyBeverages.getContentPane().add(clearButton);
		
		menuBar = new JMenuBar();
		frmBuyBeverages.setJMenuBar(menuBar);
		
		mnNewMenu = new JMenu("Profile");
		menuBar.add(mnNewMenu);
		
		mntmNewMenuItem = new JMenuItem("Main Form");
		mnNewMenu.add(mntmNewMenuItem);
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainForm2 mainForm = new MainForm2();
				mainForm.setVisibliMain(mainForm.window);
				frmBuyBeverages.dispose();
			}
		});
		
		mntmNewMenuItem_1 = new JMenuItem("Log Off");
		mnNewMenu.add(mntmNewMenuItem_1);
		mntmNewMenuItem_1.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOptionDialogActionPerformed2(evt);
                frmBuyBeverages.dispose();
            }
        });
		
		
		mntmNewMenuItem_2 = new JMenuItem("Exit");
		mnNewMenu.add(mntmNewMenuItem_2);
		mntmNewMenuItem_2.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOptionDialogActionPerformed(evt);
            }
        });
		
		
		mnNewMenu_1 = new JMenu("Transaction");
		menuBar.add(mnNewMenu_1);
		
		mntmNewMenuItem_3 = new JMenuItem("Buy Beverage");
		mnNewMenu_1.add(mntmNewMenuItem_3);
		
		mntmNewMenuItem_4 = new JMenuItem("View Transaction History");
		mnNewMenu_1.add(mntmNewMenuItem_4);
		mntmNewMenuItem_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TransactionForm transaction = new TransactionForm();
				transaction.setVisibliTransaction(transaction.window);
				frmBuyBeverages.dispose();
//				frmMainForm.setVisible(false);
			}
		});

		table_1.getColumnModel().getColumn(1).setPreferredWidth(103);
		table_1.getColumnModel().getColumn(2).setPreferredWidth(92);
		table_1.getColumnModel().getColumn(3).setPreferredWidth(90);
		table_1.getColumnModel().getColumn(4).setPreferredWidth(94);
		table_1.getColumnModel().getColumn(5).setPreferredWidth(103);
	}

	public CartTableModel refreshCartTableModel() {
		return new CartTableModel(cartController.getCartList());
	}

	private static class CartTableModel extends AbstractTableModel {
		private final String[] COLUMNS = { "Beverage Id", "Beverage Name", "Beverage Type", "Beverage Price",
				"Beverage Stock", "Beverage Quantity", "Subtotal" };
		private List<Cart> cartList;

		private CartTableModel(List<Cart> cartList) {
			this.cartList = cartList;
		}

		@Override
		public int getRowCount() {
			return cartList.size();
		}

		@Override
		public int getColumnCount() {
			return COLUMNS.length;
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			switch (columnIndex) {
			case 0:
				return cartList.get(rowIndex).getBeverageId();
			case 1:
				return cartList.get(rowIndex).getBeverageName();
			case 2:
				return cartList.get(rowIndex).getBeveragetype();
			case 3:
				return cartList.get(rowIndex).getBeveragePrice();
			case 4:
				return cartList.get(rowIndex).getBeverageStock();
			case 5:
				return cartList.get(rowIndex).getQuantity();
			case 6:
				return cartList.get(rowIndex).getSubtotal();
			default:
				return '-';
			}
		}

		@Override
		public String getColumnName(int column) {
			return COLUMNS[column];
		}

		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (getValueAt(0, columnIndex) != null) {
				return getValueAt(0, columnIndex).getClass();
			} else {
				return Object.class;
			}
		}
	}

	private static class BeverageTableModel extends AbstractTableModel {
		private final String[] COLUMNS = { "Beverage Id", "Beverage Name", "Beverage Type", "Beverage Price",
				"Beverage Stock" };
		private List<Beverage> beverageList;

		private BeverageTableModel(List<Beverage> beverageList) {
			this.beverageList = beverageList;
		}

		@Override
		public int getRowCount() {
			return beverageList.size();
		}

		@Override
		public int getColumnCount() {
			return COLUMNS.length;
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			switch (columnIndex) {
			case 0:
				return beverageList.get(rowIndex).getId();
			case 1:
				return beverageList.get(rowIndex).getName();
			case 2:
				return beverageList.get(rowIndex).getType();
			case 3:
				return beverageList.get(rowIndex).getPrice();
			case 4:
				return beverageList.get(rowIndex).getStock();
			default:
				return '-';
			}
		}

		@Override
		public String getColumnName(int column) {
			return COLUMNS[column];
		}

		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (getValueAt(0, columnIndex) != null) {
				return getValueAt(0, columnIndex).getClass();
			} else {
				return Object.class;
			}
		}
	}

}
