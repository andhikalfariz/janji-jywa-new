package utill;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import controller.BeverageController;

public class MySQLConnection {
	
	
//	private static final String URL = "jdbc:mysql://127.0.0.1:3306/janji_jiwa"; // mysql
	private static final String url = "jdbc:mariadb://localhost:3306/janji_jiwa"; // mariadb
	private static final String username = "root";
	private static final String password = "";
	
	
	public static Connection createConnection() throws SQLException, ClassNotFoundException {
		
		Connection connection = null;
		
		try {
			connection = DriverManager.getConnection(url,username,password);
			connection.setAutoCommit(false);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		return connection;

	}
}
